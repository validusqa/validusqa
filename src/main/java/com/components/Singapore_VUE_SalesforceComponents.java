package com.components;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.swing.plaf.synth.SynthSeparatorUI;

import org.apache.tools.ant.taskdefs.compilers.Sj;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.server.commandhandler.UploadFile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Screen;

import com.baseClasses.BaseClass_Web;
import com.baseClasses.PDFResultReport;
import com.baseClasses.ThreadLocalWebdriver;
import com.mobileUtilities_iOS.ThreadLocaliOSDriver;
import com.objectRepository.Singapore_VUE_Locators;
import com.objectRepository.ValidusRegisterAsInvester;
import com.objectRepository.ValidusSmoke_Loc;
import com.objectRepository.Validus_SalesforceLoc;

import bsh.org.objectweb.asm.Type;

public class Singapore_VUE_SalesforceComponents extends BaseClass_Web {
	public ValidusRegisterAsInvester registerasInvestorlocators=new ValidusRegisterAsInvester();
	
	public ValidusSmoke_Loc validussmokelocators = new ValidusSmoke_Loc();
	public Validus_SalesforceLoc salesforcelocators = new Validus_SalesforceLoc();
	public Singapore_VUE_Locators singapore_VUE_Locators=new Singapore_VUE_Locators();
	public static  int small=0;
	public static int big=0;
	public Singapore_VUE_SalesforceComponents(PDFResultReport pdfresultReport) {
		this.pdfResultReport = pdfresultReport;
	}

	public void openURL() throws Exception {
		try {
			launchapp(pdfResultReport.testDataValue.get("AppURL"));
			pdfResultReport.addStepDetails("openURL", "Application should open the url",
					"Successfully opened the URL" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to open the URL" + e.getMessage());
			pdfResultReport.addStepDetails("openURL", "Application should open the url",
					"Unable to open the URL" + e.getMessage(), "Fail", "N");
		}

	}

	public void salesforceIntegrationLogin() throws Throwable {

		try {
			Thread.sleep(2000);
			Robot r= new Robot();
		 
			 r.keyPress(KeyEvent.VK_CONTROL);
			 r.keyPress(KeyEvent.VK_T);
			 
			 Thread.sleep(2000);
			 
			 switchwindow(1);
			ThreadLocalWebdriver.getDriver().get("https://test.salesforce.com/");
		/*	set(salesforcelocators.username, "saisateam@validus.sg.dev");
			set(salesforcelocators.password, "test1234");*/
			waitForObj(2000);
			set(salesforcelocators.username, "clsupport@validus.sg.pinjamnnew");
			waitForObj(2000);
			set(salesforcelocators.password, "Validus*1");
			click(salesforcelocators.login);
pdfResultReport.addStepDetails("salesforceIntegrationLogin", "User should able to login into salesforce",
					"Successfully logged into salesforce" , "Pass", "Y");
	
			
	} catch (Exception e) {
		pdfResultReport.addStepDetails("salesforceIntegrationLogin", "User should able to login into salesforce",
				"Unable to login into salesforce" + e.getMessage(), "Fail", "N");
	}
	}
	public void salesforceLoginTest() throws Throwable {

		try {
			
			ThreadLocalWebdriver.getDriver().get("https://test.salesforce.com/");
		/*	set(salesforcelocators.username, "saisateam@validus.sg.dev");
			set(salesforcelocators.password, "test1234");*/
			waitForObj(2000);
			set(salesforcelocators.username, "clsupport@validus.sg.pinjamnnew");
			waitForObj(2000);
			set(salesforcelocators.password, "newpinjaman123");
			/*set(salesforcelocators.username, "mfiflex.code@88trialforce.com.dev");
			set(salesforcelocators.password, "Doodle1234");*/
			click(salesforcelocators.login);
			
			
	} catch (Exception e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	}
	
	
	public void salesforceSTWCIntegrationLogin() throws Throwable {

		try {
			Thread.sleep(2000);
			/*Screen s = new Screen();
			s.click("E:\\SeleniumMavenProject\\SeleniumMavenProject\\MediaFiles\\newtabs.PNG");*/
			Robot r= new Robot();
			 
			 
			 r.keyPress(KeyEvent.VK_CONTROL);
			 r.keyPress(KeyEvent.VK_T);
			 
			 Thread.sleep(2000);
			
			switchwindow(1);
			ThreadLocalWebdriver.getDriver().get("https://test.salesforce.com/");
		/*	set(salesforcelocators.username, "saisateam@validus.sg.dev");
			set(salesforcelocators.password, "test1234");*/
			/*set(salesforcelocators.username, "clsupport@validus.sg.pinjamnnew");
			set(salesforcelocators.password, "abcd12345");*/
			set(salesforcelocators.username, "mfiflex.code@88trialforce.com.vstage");
			set(salesforcelocators.password, "test1234");
			click(salesforcelocators.login);
			Thread.sleep(8000);
			
			
	} catch (Exception e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	}
	public void singaporeSalesforceIntegrationLogin() throws Throwable {

		try {
			Thread.sleep(2000);
			Robot r= new Robot();
			 r.keyPress(KeyEvent.VK_CONTROL);
			 r.keyPress(KeyEvent.VK_T);
			 Thread.sleep(2000);
			switchwindow(1);
			ThreadLocalWebdriver.getDriver().get("https://test.salesforce.com/");
			set(salesforcelocators.username, "mfiflex.code@88trialforce.com.vstage");
			set(salesforcelocators.password, "test1234");
			click(salesforcelocators.login);
			Thread.sleep(4000);
			pdfResultReport.addStepDetails("singaporeSalesforceIntegrationLogin", "Application should login into SalesForce",
					"Successfully  logged in into SalesForce" + " ", "Pass", "Y");
	} catch (Exception e1) {
		// TODO Auto-generated catch block
		pdfResultReport.addStepDetails("singaporeSalesforceIntegrationLogin", "Application should login into SalesForce",
				"Unable to  login into SalesForce" + " ", "Pass", "Y");
	}
	}
	
public void singaporeMailOTP() throws Throwable {
			
			try {
				Robot r= new Robot();
				 r.keyPress(KeyEvent.VK_CONTROL);
				 r.keyPress(KeyEvent.VK_T);
				 
				
				
				Thread.sleep(4000);
			try {
				switchwindow(2);
				Thread.sleep(1000);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			Thread.sleep(4000);
			ThreadLocalWebdriver.getDriver().get("https://www.google.com/gmail/about/#");
			click(salesforcelocators.signIN);
			Thread.sleep(3000);
			set(salesforcelocators.emailId, "autoqa@validus.sg");
			click(salesforcelocators.emailNext);
			Thread.sleep(3000);
			set(salesforcelocators.gmailPassword,"Validu$Tech");
			Thread.sleep(1000);
			click(salesforcelocators.emailNext);
			Thread.sleep(4000);
			//js_type(By.xpath("//input[@id='gbqfq']"), "noreply@salesforce.com", "search");
			set(salesforcelocators.gmailSearch, "noreply@salesforce.com");
		//	ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='gbqfq']")).sendKeys("noreply@salesforce.com");
			//ThreadLocalWebdriver.getDriver().findElement(By.xpath("//td[@style='vertical-align: top;']")).sendKeys("noreply@salesforce.com");
			
			click(salesforcelocators.gmailSearchButton);
			Thread.sleep(3000);
		List<WebElement> ele=ThreadLocalWebdriver.getDriver().findElements(By.xpath("//b[text()='Sandbox: Verify your identity in Salesforce']"));
		System.out.println(ele.size());
		for (int i = 0; i < ele.size(); i++) {
			try {
				ele.get(i).click();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//	click(salesforcelocators.gmailMail);
	String strn = ThreadLocalWebdriver.getDriver().findElement(By.xpath("(//div[@class='ii gt'])[1]")).getText();
	System.out.println(strn);
	String otp = null;
	String[] str1 = strn.split("\n");

			System.out.println(str1.length);
			for (int i = 0; i < str1.length; i++) {
				
				if(str1[i].contains("Verification")) {
					String str2 = str1[i];
					String[] str3 = str2.split(":");
					System.out.println(str3.length);
					String otps = str3[1];
					System.out.println(otps);
					
					 otp = otps.trim();
					System.out.println(otp);
					Thread.sleep(5000);
					
					
				//JSClick(salesforcelocators.gmailDelete, "Delete");
					//ThreadLocalWebdriver.getDriver().findElement(By.xpath("//div[@class='T-I J-J5-Ji nX T-I-ax7 T-I-Js-Gs  W6eDmd']")).click();
					/*s.click("C:\\Automation\\Workspace\\SeleniumAutomation_Validus\\MediaFiles\\Delete.PNG");
					Thread.sleep(2000);*/
				}
			
		
			}
			
			
			/*
			ThreadLocalWebdriver.getDriver().get("http://mail.saisasolutions.com");
			set(salesforcelocators.saisaUsername, "nithin.bolishetti@saisasolutions.com");
			set(salesforcelocators.saisaPassword, "Ni9hin@27");
			click(salesforcelocators.mailLogin);
			Thread.sleep(30000);
			switchframe(ThreadLocalWebdriver.getDriver().findElement(By.name("UserEmail")));
			String time = ThreadLocalWebdriver.getDriver()
					.findElement(By.xpath("//*[@id=\"ctl00_Split_GP_MPH_HyperGrid1_0\"]/td[3]/div[1]")).getText();
			System.out.println(time);
			String[] mins = time.split(":");

			click(By.xpath("(//div[@class=\"multiline line1 shrink\"])[1]"));
			// driver.findElement(By.xpath("(//div[@class=\"multiline line1
			// shrink\"])[1]")).click();
			Thread.sleep(5000);
			// driver.switchTo().defaultContent();
			switchframe(ThreadLocalWebdriver.getDriver().findElement(By.id("ctl00_Split_Frame_ContentFrame")));
			String otp = null;
			String str1 = ThreadLocalWebdriver.getDriver()
					.findElement(By.xpath("//*[@id='ctl00_MPH_UpdatePanel2']/pre")).getText();

			// System.out.println(str1);
			String[] s1 = str1.split("\n");
			System.out.println(s1.length);
			for (int i = 0; i < s1.length; i++) {
				if (s1[i].contains("Verification")) {
					String code = s1[i];
					System.out.println(code);
					String[] ec = code.split(":");
					otp = ec[1];
					System.out.println("code ::" + otp);
				}
			}*/
			
			
			
			ThreadLocalWebdriver.getDriver().close();
			
		
			Thread.sleep(3000);
			try {
				switchwindow(1);
				Thread.sleep(2000);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Thread.sleep(4000);
			set(By.xpath("//input[@id='emc']"), otp);
			click(By.xpath("//input[@id='save']"));
			waitForObj(6000);
			
			pdfResultReport.addStepDetails("Gmail Salesforce OTP ", "User should able to take salesforce OTP successfully",
					"Successfully user is able to take the salesforce otp from the gmail" + " ", "Pass", "Y");
		} catch (Exception e3) {
			log.fatal("Unable to take the salesforce otp from the gmail" + e3.getMessage());
			pdfResultReport.addStepDetails("Gmail Salesforce OTP", "User is not able to take the salesforce otp from the gmail",
					"Unable to take the salesforce otp from salesforce application" + e3.getMessage(), "Fail", "N");
		}
		}
			/*set(salesforcelocators.investorSearch, pdfResultReport.testData.get("SearchText"));
			click(salesforcelocators.searchButton);
			waitForObj(2000);
			click(salesforcelocators.vikasNahata);
			waitForObj(2000);
			click(salesforcelocators.dropDown);
			waitForObj(2000);
			click(salesforcelocators.userDetails);
			waitForObj(2000);
			click(salesforcelocators.loginVikasNahata);
			waitForObj(2000);
			click(salesforcelocators.application);*/

		public void salesforceOTP() throws Throwable {
	try {
				Thread.sleep(3000);
				js_type(salesforcelocators.investorSearch, ValidusSmoke_Components.emailIDInvestor, "SalesforceSearch");
				click(salesforcelocators.investorSearchButton);
			String str=	text(salesforcelocators.recordlist);
			System.out.println(str);
			String[] s2 = str.split("\n");
			System.out.println(s2.length);
			for(int j=0; j<s2.length;j++) {
				if(s2[j].contains("aApN000")) {
					String code1 = s2[j];
					System.out.println(code1);
					
					String[] abd = code1.split(" ");
					String otp2 = abd[1];
					System.out.println("Code ::" +otp2);
					ThreadLocalWebdriver.getDriver().findElement(By.xpath("//a[contains(text(),'"+otp2+"')]")).click();
				break;
				}
			}
			Thread.sleep(3000);
			String optMsg = ThreadLocalWebdriver.getDriver()
					.findElement(By.xpath("//*[@id='00NN00000039a68_ileinner']")).getText();
			System.out.println("optMsg ::" + optMsg);
			switchwindow(0);
			JavascriptExecutor js = (JavascriptExecutor) ThreadLocalWebdriver.getDriver();
			WebElement el = ThreadLocalWebdriver.getDriver().findElement(By.cssSelector("input[id=otpEntered]"));
			js.executeScript("arguments[0].value = arguments[1];", el, optMsg);
			click(salesforcelocators.continueInvestor);
			Thread.sleep(2000);
			waitForElement((By.xpath("//h2[@class='heading-tQ']")), 20);
	     	String strr=	ThreadLocalWebdriver.getDriver().findElement(By.xpath("//h2[@class='heading-tQ']")).getText();
	     	System.out.println(strr);
	     	ThreadLocalWebdriver.getDriver().close();
			switchwindow(0);
			Actions a = new Actions(ThreadLocalWebdriver.getDriver());
			a.sendKeys(Keys.F5).build().perform();
			Robot r=new Robot();
			r.keyPress(KeyEvent.VK_F5);
			r.keyRelease(KeyEvent.VK_F5);
			
			waitForObj(3000);
			
			ThreadLocalWebdriver.getDriver().navigate().to(ThreadLocalWebdriver.getDriver().getCurrentUrl());
			waitForObj(1000);
			ThreadLocalWebdriver.getDriver().navigate().to(ThreadLocalWebdriver.getDriver().getCurrentUrl());
			waitForObj(2000);
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//a[contains(text(),'http://batumbu.com/pinjaman_bahasa_enkripsi/pla...')]")).click();
				switchwindow(1);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='proceed']")).click();
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//a[contains(text(),'login ke Platform Batumbu.')]")).click();
			//	switchwindow(2);
	
				waitForObj(4000);
				pdfResultReport.addStepDetails("Salesforce OTP", "User should able to verify mobile and email successfully",
						"Successfully verified the email and mobile from the salesforce application" + " ", "Pass", "Y");
			} 
	catch (Exception e3) {
		log.fatal("Unable to verify the mobile and email verification in Salesforce" + e3.getMessage());
		pdfResultReport.addStepDetails("Salesforce OTP", "User is not able to verify mobile and email from salesforce",
				"Unable to verify the email and mobile from salesforce application" + e3.getMessage(), "Fail", "N");
	}
		
			
		}
	
		public void salesforceOTPBatumbu_SME() throws Throwable {
			try {
						Thread.sleep(3000);
						js_type(salesforcelocators.investorSearch, ValidusRegisterAsInvestorComponents.emailIDInvestor, "SalesforceSearch");
						click(salesforcelocators.investorSearchButton);
					String str=	text(salesforcelocators.recordlist);
					System.out.println(str);
					String[] s2 = str.split("\n");
					System.out.println(s2.length);
					for(int j=0; j<s2.length;j++) {
						if(s2[j].contains("aApN000")) {
							String code1 = s2[j];
							System.out.println(code1);
							
							String[] abd = code1.split(" ");
							String otp2 = abd[1];
							System.out.println("Code ::" +otp2);
							ThreadLocalWebdriver.getDriver().findElement(By.xpath("//a[contains(text(),'"+otp2+"')]")).click();
						break;
						}
					}
					Thread.sleep(3000);
					String optMsg = ThreadLocalWebdriver.getDriver()
							.findElement(By.xpath("//*[@id='00NN00000039a68_ileinner']")).getText();
					System.out.println("optMsg ::" + optMsg);
					switchwindow(0);
					JavascriptExecutor js = (JavascriptExecutor) ThreadLocalWebdriver.getDriver();
					WebElement el = ThreadLocalWebdriver.getDriver().findElement(By.cssSelector("input[id=otpEntered]"));
					js.executeScript("arguments[0].value = arguments[1];", el, optMsg);
					click(salesforcelocators.continueInvestor);
					Thread.sleep(2000);
					waitForElement((By.xpath("//h2[@class='heading-tQ']")), 20);
			     	String strr=	ThreadLocalWebdriver.getDriver().findElement(By.xpath("//h2[@class='heading-tQ']")).getText();
			     	System.out.println(strr);
			     	ThreadLocalWebdriver.getDriver().close();
					switchwindow(0);
					Actions a = new Actions(ThreadLocalWebdriver.getDriver());
					a.sendKeys(Keys.F5).build().perform();
					Robot r=new Robot();
					r.keyPress(KeyEvent.VK_F5);
					r.keyRelease(KeyEvent.VK_F5);
					
					waitForObj(3000);
					
					ThreadLocalWebdriver.getDriver().navigate().to(ThreadLocalWebdriver.getDriver().getCurrentUrl());
					waitForObj(1000);
					ThreadLocalWebdriver.getDriver().navigate().to(ThreadLocalWebdriver.getDriver().getCurrentUrl());
					waitForObj(2000);
						ThreadLocalWebdriver.getDriver().findElement(By.xpath("//a[contains(text(),'http://batumbu.com/pinjaman_bahasa_enkripsi/pla...')]")).click();
						switchwindow(1);
					ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='proceed']")).click();
						ThreadLocalWebdriver.getDriver().findElement(By.xpath("//a[contains(text(),'login ke Platform Batumbu.')]")).click();
					//	switchwindow(2);
			
						waitForObj(4000);
	pdfResultReport.addStepDetails("salesforceOTPBatumbu_SME", "User should able to verify mobile and email successfully",
								"Successfully verified the email and mobile from the salesforce application" + " ", "Pass", "Y");
					} 
			catch (Exception e3) {
				log.fatal("Unable to verify the mobile and email verification in Salesforce" + e3.getMessage());
				pdfResultReport.addStepDetails("salesforceOTPBatumbu_SME", "User should able to verify mobile and email successfully",
						"Unable to verify the email and mobile from salesforce application" + e3.getMessage(), "Fail", "N");
			}
				
					
				}
			
		public void salesforceOTPBatumbu_SME_NEW() throws Throwable {
			try {
						Thread.sleep(3000);
					//	js_type(salesforcelocators.investorSearch, ValidusRegisterAsInvestorComponents.emailIDInvestor, "SalesforceSearch");
						click(By.xpath("//li[@id='MoreTabs_Tab']"));
						Thread.sleep(1000);
						click(By.xpath("//li/a[text()='Unregistered Users']"));
						Thread.sleep(5000);
						click(By.xpath("(//table[@class='list']//tr//th/a)[1]"));
						Thread.sleep(3000);
					Thread.sleep(3000);
					String optMsg = ThreadLocalWebdriver.getDriver()
							.findElement(By.xpath("//*[@id='00NN00000039a68_ileinner']")).getText();
					System.out.println("optMsg ::" + optMsg);
					switchwindow(0);
					JavascriptExecutor js = (JavascriptExecutor) ThreadLocalWebdriver.getDriver();
					WebElement el = ThreadLocalWebdriver.getDriver().findElement(By.cssSelector("input[id=otpEntered]"));
					js.executeScript("arguments[0].value = arguments[1];", el, optMsg);
					click(salesforcelocators.continueInvestor);
					Thread.sleep(2000);
					waitForElement((By.xpath("//h2[@class='heading-tQ']")), 20);
			     	String strr=	ThreadLocalWebdriver.getDriver().findElement(By.xpath("//h2[@class='heading-tQ']")).getText();
			     	System.out.println(strr);
			     	ThreadLocalWebdriver.getDriver().close();
					switchwindow(0);
					Actions a = new Actions(ThreadLocalWebdriver.getDriver());
					a.sendKeys(Keys.F5).build().perform();
					Robot r=new Robot();
					r.keyPress(KeyEvent.VK_F5);
					r.keyRelease(KeyEvent.VK_F5);
					
					waitForObj(3000);
					
					ThreadLocalWebdriver.getDriver().navigate().to(ThreadLocalWebdriver.getDriver().getCurrentUrl());
					waitForObj(1000);
					ThreadLocalWebdriver.getDriver().navigate().to(ThreadLocalWebdriver.getDriver().getCurrentUrl());
					waitForObj(2000);
						ThreadLocalWebdriver.getDriver().findElement(By.xpath("//a[contains(text(),'https://demoplatform.batumbu.id/index.php/home/...')]")).click();
						switchwindow(1);
					ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='proceed']")).click();
						ThreadLocalWebdriver.getDriver().findElement(By.xpath("//a[contains(text(),'login ke Platform Batumbu.')]")).click();
					//	switchwindow(2);
			
						waitForObj(4000);
	pdfResultReport.addStepDetails("salesforceOTPBatumbu_SME", "User should able to verify mobile and email successfully",
								"Successfully verified the email and mobile from the salesforce application" + " ", "Pass", "Y");
					} 
			catch (Exception e3) {
				log.fatal("Unable to verify the mobile and email verification in Salesforce" + e3.getMessage());
				pdfResultReport.addStepDetails("salesforceOTPBatumbu_SME", "User should able to verify mobile and email successfully",
						"Unable to verify the email and mobile from salesforce application" + e3.getMessage(), "Fail", "N");
			}
				
					
				}
				
		public void singaporeSalesforceOTP() throws Throwable {
			try {
				Thread.sleep(2000);
				js_type(salesforcelocators.investorSearch, Singapore_VUE_Components.emailIDInvestor,"SG Search");
					click(salesforcelocators.investorSearchButton);
					String str=	text(salesforcelocators.recordlist);
					System.out.println(str);
					String[] s2 = str.split("\n");
					System.out.println(s2.length);
					for(int j=0; j<s2.length;j++) {
						if(s2[j].contains("a770")) {
							String code1 = s2[j];
							System.out.println(code1);
							
							String[] abd = code1.split(" ");
							String otp2 = abd[1];
							System.out.println("Code ::" +otp2);
							ThreadLocalWebdriver.getDriver().findElement(By.xpath("//a[contains(text(),'"+otp2+"')]")).click();
						}
					}
					Thread.sleep(2000);
					String optMsg = ThreadLocalWebdriver.getDriver()
							.findElement(By.xpath("//*[@id='00N28000009zdXG_ileinner']")).getText();
					System.out.println("optMsg :::" + optMsg);
					switchwindow(0);
					try {
						Thread.sleep(2000);
						String otp1="";
						String otp2="";
					//	set(By.xpath("//input[@placeholder='Please enter OTP']"), optMsg);
						click(salesforcelocators.otp);
						
				/*		for(int i=0 ;i<3; i++) {
							otp1=otp1+optMsg.charAt(i);
						}
						System.out.println("otp1 : "+otp1);
						ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@placeholder='Please enter OTP']")).sendKeys(otp1.trim());
						Thread.sleep(1000);
						tab();
						click(By.xpath("//input[@placeholder='Please enter OTP']"));
						for(int j=3 ;j<6; j++) {
							otp2=otp2+optMsg.charAt(j);
						}*/
				
						Screen s=new Screen();
						Robot r=new Robot();
						r.keyPress(KeyEvent.VK_CONTROL);
						r.keyRelease(KeyEvent.VK_CONTROL);
						Thread.sleep(2000);
						r.keyPress(KeyEvent.VK_CONTROL);
						r.keyRelease(KeyEvent.VK_CONTROL);
						Thread.sleep(2000);
					//	ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@placeholder='Please enter OTP']")).sendKeys(optMsg);
					//	Thread.sleep(2000);
						//ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@placeholder='Please enter OTP']")).clear();
					//	Thread.sleep(2000);
						s.type(System.getProperty("user.dir").replace("\\", "/") + "/MediaFiles/otp.png", optMsg);
						Thread.sleep(2000);
						/*	s.type(System.getProperty("user.dir").replace("\\", "/") + "/MediaFiles/otp.png", optMsg);
						Thread.sleep(2000);*/
					click(salesforcelocators.verify);
						Thread.sleep(2000);
						
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				//	click(singapore_VUE_Locators.continueBtn);
					Thread.sleep(2000);
			    // 	String strr=	ThreadLocalWebdriver.getDriver().findElement(By.xpath("//*[@id='pane-step2']/div/h2")).getText();
			     //	System.out.println(strr);
			     	ThreadLocalWebdriver.getDriver().close();
					switchwindow(0);
					Actions a = new Actions(ThreadLocalWebdriver.getDriver());
					a.sendKeys(Keys.F5).build().perform();
					Robot r=new Robot();
					r.keyPress(KeyEvent.VK_F5);
					r.keyRelease(KeyEvent.VK_F5);
					
					waitForObj(2000);
					pdfResultReport.addStepDetails("Salesforce mobile and email verification", "User should able to verify mobile and email successfully",
							"Successfully verified the email and mobile from the salesforce application" + " ", "Pass", "Y");
				
					ThreadLocalWebdriver.getDriver().navigate().to(ThreadLocalWebdriver.getDriver().getCurrentUrl());
					waitForObj(1000);
					ThreadLocalWebdriver.getDriver().navigate().to(ThreadLocalWebdriver.getDriver().getCurrentUrl());
					waitForObj(2000);
					click(salesforcelocators.emailVerificationLink);
						switchwindow(1);
						waitForObj(5000);
						click(salesforcelocators.loginNow);
						} 
			catch (Exception e3) {
				log.fatal("Unable to verify the mobile and email verification in Salesforce" + e3.getMessage());
				pdfResultReport.addStepDetails("Salesforce mobile and email verification", "User is not able to verify mobile and email from salesforce",
						"Unable to verify the email and mobile from salesforce application" + e3.getMessage(), "Fail", "N");
			}
	}
		
		public void singapore_SME_SalesforceOTP() throws Throwable {
			try {
				
     				js_type(salesforcelocators.investorSearch, Singapore_SME_PHP_Components.emailIDInvestor,"SG Search");
//	submit(salesforcelocators.investorSearch);
     				Thread.sleep(5000);
					click(salesforcelocators.investorSearchButton);				
     			//	click(salesforcelocators.unRegisteredUSername);
					String str=	text(salesforcelocators.recordlist);
					System.out.println(str);
					String[] s2 = str.split("\n");
					System.out.println(s2.length);
					for(int j=0; j<s2.length;j++) {
						if(s2[j].contains("a770")) {
							String code1 = s2[j];
							System.out.println(code1);
							
							String[] abd = code1.split(" ");
							String otp2 = abd[1];
							System.out.println("Code ::" +otp2);
							ThreadLocalWebdriver.getDriver().findElement(By.xpath("//a[contains(text(),'"+otp2+"')]")).click();
						}
					}
					Thread.sleep(3000);
					String optMsg = ThreadLocalWebdriver.getDriver()
							.findElement(By.xpath("//*[@id='00N28000009zdXG_ileinner']")).getText();
					System.out.println("optMsg ::" + optMsg);
					switchwindow(0);
					try {
						set(By.xpath("//input[@id='otpEntered']"), optMsg);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					click(validussmokelocators.continueBtn);
					Thread.sleep(2000);
			     	String strr=	ThreadLocalWebdriver.getDriver().findElement(By.xpath("//div[@class=\"v-step-box\"]//div//h2")).getText();
			     	System.out.println(strr);
			     	ThreadLocalWebdriver.getDriver().close();
					switchwindow(0);
					Actions a = new Actions(ThreadLocalWebdriver.getDriver());
					a.sendKeys(Keys.F5).build().perform();
					Robot r=new Robot();
					r.keyPress(KeyEvent.VK_F5);
					r.keyRelease(KeyEvent.VK_F5);
					
					waitForObj(3000);
					pdfResultReport.addStepDetails("Salesforce mobile and email verification", "User should able to verify mobile and email successfully",
							"Successfully verified the email and mobile from the salesforce application" + " ", "Pass", "Y");
				
					ThreadLocalWebdriver.getDriver().navigate().to(ThreadLocalWebdriver.getDriver().getCurrentUrl());
					waitForObj(1000);
					ThreadLocalWebdriver.getDriver().navigate().to(ThreadLocalWebdriver.getDriver().getCurrentUrl());
					waitForObj(2000);
						ThreadLocalWebdriver.getDriver().findElement(By.xpath("//a[contains(text(),'http://13.232.62.150/validuscldev/platform/inde...')]")).click();
						switchwindow(1);
						ThreadLocalWebdriver.getDriver().findElement(By.linkText("login to the Validus Platform.")).click();
						} 
			catch (Exception e3) {
				log.fatal("Unable to verify the mobile and email verification in Salesforce" + e3.getMessage());
				pdfResultReport.addStepDetails("Salesforce mobile and email verification", "User is not able to verify mobile and email from salesforce",
						"Unable to verify the email and mobile from salesforce application" + e3.getMessage(), "Fail", "N");
			}
				
					
				}
		
		
	public void salesforceApproval() throws Throwable {
		
		try {
			System.out.println(".............  Started executing the method salesforceApproval ...............");
		waitForObj(2000);
		switchwindow(0);
	//	js_type(salesforcelocators.investorSearch, "nag0614115309@gmailx.com","Salesforce Search");
		js_type(salesforcelocators.investorSearch, Singapore_VUE_Components.emailIDInvestor,"Salesforce Search");
		waitForObj(4000);
		click(salesforcelocators.investorSearchButton);
	    String str=	text(salesforcelocators.salesforceApplications);
		System.out.println(str);
		String[] s = str.split("\n");
		System.out.println(s.length);
		for(int i=0;i<s.length;i++) {
			
			if(s[i].contains("APP-000")) {
				String x = s[i];
				System.out.println(x);
				String[] y = x.split(" ");
				String z = y[1];
				System.out.println(z);
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//a[contains(text(),'"+z+"')]")).click();
				Thread.sleep(6000);
				try {
					System.out.println("steped ...........................");
					switchframe(ThreadLocalWebdriver.getDriver().findElement(By.id("contentPane")));
					waitForElement(salesforcelocators.singaporeApproveLink, 5);
					JSClick(salesforcelocators.singaporeApproveLink, "singaporeApproveLink");
					switchbacktodefaultframe();
				} catch (Exception e) {
					JSClick(salesforcelocators.singaporeApproveLink, "singaporeApproveLink");
					System.out.println("In Catch it clicked ...........................");
					//switchbacktodefaultframe();
					
				}
				
				Screen c = new Screen();
				try {
					switchframe(ThreadLocalWebdriver.getDriver().findElement(By.id("contentPane")));
					click(By.xpath("//input[@id='j_id0:wizId:j_id3:j_id59:j_id60:j_id61:j_id62']"));
					Thread.sleep(2000);
					switchbacktodefaultframe();
					System.out.println("clicked on choose 1************");
				} catch (Exception e2) {
					System.out.println("unable to click on choose 1************");
				}
				
				try {
					click(By.xpath("//input[@id='j_id0:wizId:j_id3:j_id59:j_id60:j_id61:j_id62']"));
					Thread.sleep(2000);
					System.out.println("clicked on choose 2************");
				} catch (Exception e2) {
					System.out.println("unable to click on choose 2************");
				}
				Thread.sleep(2000);
				Robot r=new Robot();
				r.keyPress(KeyEvent.VK_CONTROL);
				r.keyRelease(KeyEvent.VK_CONTROL);
				c.click(System.getProperty("user.dir").replace("\\", "/") + "/MediaFiles/pdffileLinux.png");
				Thread.sleep(2000);
				r.keyPress(KeyEvent.VK_ENTER);
				r.keyRelease(KeyEvent.VK_ENTER);
				Thread.sleep(3000);
				try {
					
					switchframe(ThreadLocalWebdriver.getDriver().findElement(By.id("contentPane")));
					JSClick(salesforcelocators.singaporeUploadLink, "singaporeUploadLink");
					switchbacktodefaultframe();
					Thread.sleep(5000);
					System.out.println("In try - second try it clicked on upload..........................");
				} catch (Exception e1) {
					System.out.println("In Catch - secnd try --unable to click on upload..........................");
				}
				try {
					//	waitForElement(salesforcelocators.singaporeUploadLink, 1);
						JSClick(salesforcelocators.singaporeUploadLink, "singaporeUploadLink");
						Thread.sleep(5000);
						System.out.println("In try - first try --unable to click on upload..........................");
					} catch (Exception e) {
						System.out.println("In Catch - first try --unable to click on upload..........................");
					
					}
				
				try {
					switchframe(ThreadLocalWebdriver.getDriver().findElement(By.id("contentPane")));
					waitForElement(salesforcelocators.singaporeApproved, 3);
					JSClick(salesforcelocators.singaporeApproved, "singaporeApproved");
					switchbacktodefaultframe();
				} catch (Exception e) {
					System.out.println("In Catch it clicked on Approve..........................");
					JSClick(salesforcelocators.singaporeApproved, "singaporeApproved");
				}
				waitForObj(8000);
				pdfResultReport.addStepDetails("Salesforce Approval", "User should able to approve the record successfully",
						"Successfully approve the record from the salesforce application" + " ", "Pass", "Y");
				switchwindow(1);
				waitForObj(2000);
				
			}
		}
	
	} catch (Exception e3) {
		log.fatal("Unable to approve the record with Salesforce" + e3.getMessage());
		pdfResultReport.addStepDetails("Salesforce Approval", "User is not able to approve the record from salesforce",
				"Unable to approve the record from salesforce application" + e3.getMessage(), "Fail", "N");
	}
							
			
	
	}
	
	public void smeSalesforceApproval_firstACID() throws Exception {
		try {
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//a[contains(text(),'"+"APP-000000"+small+"')]")).click();
			waitForElement(By.xpath("//td[contains(text(),'Verification')]"), 20);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//td[contains(text(),'Verification')]")).click();
			Thread.sleep(5000);
			Screen c = new Screen();
			c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/choose.PNG");
			Thread.sleep(2000);
			c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/openn.png");
			c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/AutomationApp.png");
			
			c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/cancel.png");
			Thread.sleep(2000);
			c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/choose.PNG");
			Thread.sleep(2000);
			c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/salesforcepdf.png");
			Thread.sleep(1000);
			c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/openn.png");
			Thread.sleep(4000);
			c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/test.png");
			Thread.sleep(10000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//td[contains(text(),'Verification')]")).click();
			Thread.sleep(8000);
			refresh();
			c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/refresh.png");
			Thread.sleep(3000);
			pagedown();
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//td[contains(text(),'Verification')]")).click();
			Thread.sleep(2000);
			pagedown();
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@value='Approve']")).click();
			Thread.sleep(10000);
		} catch (Exception e) {
			System.out.println("The method smeSalesforceApproval_firstACID failed ::"+e.getMessage());
		}
	}
	public void smeSalesforceApproval_secondACID_LoanDocs1() throws Throwable {
		try {
			js_type(salesforcelocators.investorSearch, "APP-000000"+big, "SFSearch");
			waitForObj(3000);
			click(salesforcelocators.investorSearchButton);
			   System.out.println("Big number is ::"+big);
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//a[contains(text(),'"+"APP-000000"+big+"')]")).click();
				Thread.sleep(5000);
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//*[@id='j_id0:wizId:j_id3']/div[1]/div[4]/a")).click();
				Thread.sleep(5000);
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='j_id0:j_id33:j_id34:j_id35:j_id37:j_id38:j_id49']")).click();
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@value='Save']")).click();
				Thread.sleep(10000);
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//td[contains(text(),'Loan Documents')]")).click();
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id115']")).click();
				Thread.sleep(4000);
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//td[contains(text(),'Loan Documents')]")).click();
				Thread.sleep(4000);
		} catch (Exception e) {
			System.out.println("The method smeSalesforceApproval_secondACID_LoanDocs1 is failed::"+e.getMessage());
		}
	}
	public void smeSalesforceApproval_secondACID_LoanDocs2() throws Throwable {
	try {
			refresh();
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:0:j_id95']")).click();
			
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:1:j_id95']")).click();
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:2:j_id95']")).click();
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:3:j_id95']")).click();
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:4:j_id95']")).click();
		//	ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:5:j_id95']")).click();
		//	Thread.sleep(4000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id111']")).click();
			Thread.sleep(4000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//td[contains(text(),'Promissory Note')]")).click();
			Thread.sleep(4000);
			System.out.println("Entered the details in Loan documents of Salesforce");
		
	} catch (Exception e) {
		System.out.println("The method smeSalesforceApproval_secondACID_LoanDocs2 is failed::"+e.getMessage());
	}
			
		}
	public void salesforceApprovalSME() throws Throwable {
		
		try {
			String firstnum = null;
			String secondnum = null;
			switchwindow(0);
		waitForObj(2000);
		js_type(salesforcelocators.investorSearch, ValidusRegisterAsInvestorComponents.emailIDInvestor, "SFSearch");
	//	js_type(salesforcelocators.investorSearch, "nag0208031535@gmailx.com", "SFSearch");
		waitForObj(1000);
		click(salesforcelocators.investorSearchButton);
		waitForObj(3000);
		Screen c=new Screen();
		c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/refresh.png");
		Thread.sleep(3000);
		js_type(salesforcelocators.investorSearch, ValidusRegisterAsInvestorComponents.emailIDInvestor, "SFSearch");
			waitForObj(1000);
			click(salesforcelocators.investorSearchButton);
			waitForObj(3000);
		
	    String str=	text(salesforcelocators.salesforceApplications);
		System.out.println(str);
		String[] s = str.split("\n");
		System.out.println(s.length);
		String[] appIDs=new String[2];
		int n=0;
		for(int i=0;i<s.length;i++) {
			System.out.println("account names after split:"+s[i]);
			if(s[i].contains("APP-000")) {
				n++;
				if(n==1) {
					String x=s[i];
				System.out.println(x);
				String[] y = x.split(" ");
				String z = y[1];
				firstnum=z;
				System.out.println("First application id::"+firstnum);
				
			}
				if(n==2) {
					String x=s[i];
				System.out.println(x);
				String[] y = x.split(" ");
				String z = y[1];
				secondnum=z;
				System.out.println("Second application id::"+secondnum);
				
			}
		}
		}
		 String app1=firstnum;
		  String app2=secondnum;
		   String numberOnly= app1.replaceAll("[^0-9]", "");
		   String numberOnly1= app2.replaceAll("[^0-9]", "");
		   System.out.println(numberOnly);
		   System.out.println(numberOnly1);
		   int num1=Integer.parseInt(numberOnly);
		   int num2=Integer.parseInt(numberOnly1);
		   
		  
	      if(num1 < num2)
	      {
	          small = num1;
	      }
	      else
	      {
	    	  small = num2;
	      }
		  if(num1 > num2)
	      {
	    	  big = num1;
	      }
	      else
	      {
	    	  big = num2;
	      }
			
	      System.out.print("big no of  Two Number is " +big);
	      System.out.print("small no of  Two Number is " +small);
	      smeSalesforceApproval_firstACID();
	      smeSalesforceApproval_secondACID_LoanDocs1();
	      smeSalesforceApproval_secondACID_LoanDocs2();
	      salesforceApprovalPromisoryNote();
		Thread.sleep(4000);
				refresh();
				Thread.sleep(4000);
		ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='j_id0:j_id119:j_id120:j_id121:j_id122:j_id138:j_id139:upldbtn1']")).click();
		ThreadLocalWebdriver.getDriver().findElement(By.xpath("//*[@id='j_id0:wizId:j_id3']/div[1]/div[6]/a")).click();
		Thread.sleep(4000);
		ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='j_id0:wizId:j_id4:editBtn']")).click();
		Thread.sleep(4000);
		ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='j_id0:wizId:j_id4:j_id60:j_id61:j_id66']")).sendKeys("3");
		new Select(ThreadLocalWebdriver.getDriver().findElement(By.xpath("//select[@id='j_id0:wizId:j_id4:j_id60:j_id61:j_id70']"))).selectByVisibleText("3");
		new Select(ThreadLocalWebdriver.getDriver().findElement(By.xpath("//select[@id='j_id0:wizId:j_id4:j_id60:j_id61:j_id76']"))).selectByVisibleText("SINGLE-PAYMENT");
		new Select(ThreadLocalWebdriver.getDriver().findElement(By.xpath("//select[@id='j_id0:wizId:j_id4:j_id60:j_id61:j_id74']"))).selectByVisibleText("Flat");
		js_type(By.xpath("//input[@id='j_id0:wizId:j_id4:j_id60:j_id61:j_id84']"), "15000", "Invoice Amount");
		ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@value='Save']")).click();
		Thread.sleep(10000);
		
		click(By.xpath("//input[@value='Facility Request to Investors']"));
		ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@value='Approve']")).click();
		Thread.sleep(10000);
		
		new Select(ThreadLocalWebdriver.getDriver().findElement(By.xpath("//select[@id='j_id0:wizId:j_id4:j_id245:j_id250:j_id251:j_id254']"))).selectByVisibleText("All Investors");
		ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@value='Save']")).click();
		Thread.sleep(6000);
		ThreadLocalWebdriver.getDriver().findElement(By.xpath("//div[@align='center']//span/input[@value='List to MarketPlace']")).click();
		new Select(ThreadLocalWebdriver.getDriver().findElement(By.xpath("//select[@id='j_id0:wizId:j_id4:j_id264:TimeBasedMPStatus:AmountSectionItem:mpstatus']"))).selectByVisibleText("Now");
		ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@value='Save']")).click();
		Thread.sleep(6000);

		ThreadLocalWebdriver.getDriver().switchTo().alert().accept();
		Thread.sleep(8000);
		if(ThreadLocalWebdriver.getDriver().findElement(By.xpath("//div[@id='j_id0:wizId:j_id4:j_id34:j_id35:j_id36:0:j_id37:j_id38:j_id40']")).isDisplayed()) {
		System.out.println("Loan Application Created successfully");	
		
		}  
		pdfResultReport.addStepDetails("Salesforce Approval", "User should able to approve the record successfully",
				"Successfully approved the SME record from the salesforce application" + " ", "Pass", "Y");
	} catch (Exception e3) {
		log.fatal("Unable to approve the record with Salesforce" + e3.getMessage());
		pdfResultReport.addStepDetails("Salesforce Approval", "User is not able to approve the record from salesforce",
				"Unable to approve the SME record from salesforce application" + e3.getMessage(), "Fail", "N");
	}
	
	}
	public void salesforceApprovalPromisoryNote() throws Exception {
		try {
			click(salesforcelocators.promissoryNote);
			Thread.sleep(3000);
			Screen c=new Screen();
			c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/choose.PNG");
			Thread.sleep(2000);
			c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/openn.png");
			c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/AutomationApp.png");
			
			c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/cancel.png");
			Thread.sleep(2000);
			c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/choose.PNG");
			Thread.sleep(2000);
			c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/salesforcepdf.png");
			Thread.sleep(1000);
			c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/openn.png");
			Thread.sleep(4000);
			click(salesforcelocators.promissoryNote);
		} catch (Exception e) {
			System.out.println("The method salesforceApprovalPromisoryNote is failed ::"+e.getMessage());
		}
	}
	
public void salesforceApprovalSME_New() throws Throwable {
		
		try {
			switchwindow(0);
		waitForObj(2000);
		js_type(salesforcelocators.investorSearch, ValidusRegisterAsInvestorComponents.emailIDInvestor, "SFSearch");
		//js_type(salesforcelocators.investorSearch, "nag0206125250@gmailx.com", "SFSearch");
		waitForObj(3000);
		click(salesforcelocators.investorSearchButton);
	    String str=	text(salesforcelocators.salesforceApplications);
		System.out.println(str);
		String[] s = str.split("\n");
		System.out.println(s.length);
		
		   
		   
		for(int i=0;i<s.length;i++) {
			System.out.println("account names after split:"+s[i]);
			

			/*  String str="APP-0000003639";
			  String str1="APP-0000003640";
			   String numberOnly= str.replaceAll("[^0-9]", "");
			   String numberOnly1= str1.replaceAll("[^0-9]", "");
			   System.out.println(numberOnly);
			   System.out.println(numberOnly1);
			   int num1=Integer.parseInt(numberOnly);
			   int num2=Integer.parseInt(numberOnly1);
			   if(num1<num2) {
				   System.out.println("num1 is less num2");
			   }else {
				   System.out.println("num2 is less num1");
			   }*/
			if(s[i].contains("APP-000")) {
				String x = s[i];
				System.out.println(x);
				String[] y = x.split(" ");
				String z = y[1];
				System.out.println("First application id::"+z);
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//a[contains(text(),'"+z+"')]")).click();
				waitForElement(By.xpath("//td[contains(text(),'Verification')]"), 20);
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//td[contains(text(),'Verification')]")).click();
				Thread.sleep(5000);
				Screen c = new Screen();
				c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/choose.PNG");
				Thread.sleep(2000);
				c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/openn.png");
				c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/AutomationApp.png");
				
				c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/cancel.png");
				Thread.sleep(2000);
				c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/choose.PNG");
				Thread.sleep(2000);
				c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/salesforcepdf.png");
				Thread.sleep(1000);
			//	c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/salesforcepdf.png");
			//	c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/salesforcepdf1.png");
				
				c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/openn.png");
	
				Thread.sleep(4000);
				c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/test.png");
				Thread.sleep(10000);
				switchwindow(2);
				
		//	ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@value='Upload']")).click();
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//td[contains(text(),'Verification')]")).click();
				Thread.sleep(8000);
				refresh();
				c.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/refresh.png");
				Thread.sleep(3000);
				pagedown();
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//td[contains(text(),'Verification')]")).click();
				Thread.sleep(2000);
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@value='Approve']")).click();
				Thread.sleep(10000);
			//	ThreadLocalWebdriver.getDriver().close();
				break;
				
			}
				
				
		}
			System.out.println("Working on seconde record");
			//switchwindow(0);
		js_type(salesforcelocators.investorSearch, ValidusRegisterAsInvestorComponents.emailIDInvestor, "SFSearch");
		//	js_type(salesforcelocators.investorSearch, "nag0206050546@gmailx.com", "SFSearch");
				waitForObj(4000);
				click(salesforcelocators.investorSearchButton);

				String str1=	text(salesforcelocators.salesforceApplications);
				System.out.println(str1);
				String[] ss = str1.split("\n");
				System.out.println(ss.length);
				for(int j=4;j<ss.length;j++) {
					
					if(ss[j].contains("APP-000")) {
						String xx = s[j];
						System.out.println(xx);
						String[] yy = xx.split(" ");
						String zz = yy[1];
						System.out.println("Second application id::"+zz);
						ThreadLocalWebdriver.getDriver().findElement(By.xpath("//a[contains(text(),'"+zz+"')]")).click();
						Thread.sleep(5000);
						switchwindow(3);
						Thread.sleep(2000);
						ThreadLocalWebdriver.getDriver().findElement(By.xpath("//*[@id='j_id0:wizId:j_id3']/div[1]/div[4]/a")).click();
						Thread.sleep(5000);
						ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='j_id0:j_id33:j_id34:j_id35:j_id37:j_id38:j_id49']")).click();
						ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@value='Save']")).click();
						Thread.sleep(10000);
						ThreadLocalWebdriver.getDriver().findElement(By.xpath("//td[contains(text(),'Loan Documents')]")).click();
						ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id115']")).click();
						Thread.sleep(4000);
						ThreadLocalWebdriver.getDriver().findElement(By.xpath("//td[contains(text(),'Loan Documents')]")).click();
						Thread.sleep(4000);
						try {
							refresh();
							ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:0:j_id95']")).click();
							
							ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:1:j_id95']")).click();
							ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:2:j_id95']")).click();
							ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:3:j_id95']")).click();
							ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:4:j_id95']")).click();
							ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:5:j_id95']")).click();
ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:6:j_id95']")).click();
//	ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:7:j_id95']")).click();
//	ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:8:j_id95']")).click();
							/*ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:9:j_id95']")).click();
							ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:10:j_id95']")).click();*/
							Thread.sleep(4000);
							ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id111']")).click();
							Thread.sleep(4000);
							ThreadLocalWebdriver.getDriver().findElement(By.xpath("//td[contains(text(),'Promissory Note')]")).click();
							Thread.sleep(4000);
							System.out.println("Entered the details in Loan documents of Salesforce");
						} catch (Exception e) {
							System.out.println("Unable to enter the details in Loan documents of Salesforce");
						}
						uploadFile(salesforcelocators.promissoryNote);
						Thread.sleep(4000);
						refresh();
						ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='j_id0:j_id119:j_id120:j_id121:j_id122:j_id138:j_id139:upldbtn1']")).click();
						
						
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//*[@id='j_id0:wizId:j_id3']/div[1]/div[6]/a")).click();
				Thread.sleep(4000);
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='j_id0:wizId:j_id4:editBtn']")).click();
				Thread.sleep(4000);
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='j_id0:wizId:j_id4:j_id60:j_id61:j_id66']")).sendKeys("0.65");
				new Select(ThreadLocalWebdriver.getDriver().findElement(By.xpath("//select[@id='j_id0:wizId:j_id4:j_id60:j_id61:j_id70']"))).selectByVisibleText("3");
				new Select(ThreadLocalWebdriver.getDriver().findElement(By.xpath("//select[@id='j_id0:wizId:j_id4:j_id60:j_id61:j_id76']"))).selectByVisibleText("SINGLE-PAYMENT");
				new Select(ThreadLocalWebdriver.getDriver().findElement(By.xpath("//select[@id='j_id0:wizId:j_id4:j_id60:j_id61:j_id74']"))).selectByVisibleText("Flat");
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='j_id0:wizId:j_id4:j_id60:j_id61:j_id84']")).sendKeys("15000");
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='j_id0:wizId:j_id4:j_id60:j_id61:j_id88']")).sendKeys("Test");
				
				Thread.sleep(4000);
				uploadFile(salesforcelocators.acra_Biz_file);
				Thread.sleep(4000);
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='j_id0:wizId:j_id4:j_id113:j_id114:j_id116:upldbtn1']")).click();
				Thread.sleep(4000);
				uploadFile(salesforcelocators.financialDocuments);
				Thread.sleep(4000);
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='j_id0:wizId:j_id4:j_id113:j_id127:j_id129:upldbtn2']")).click();
				Thread.sleep(4000);
				uploadFile(salesforcelocators.paymentRating);
				Thread.sleep(4000);
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='j_id0:wizId:j_id4:j_id113:j_id140:j_id142:upldbtn3']")).click();
				Thread.sleep(4000);
				uploadFile(salesforcelocators.noticeRedirect);
				Thread.sleep(4000);
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='j_id0:wizId:j_id4:j_id113:j_id153:j_id155:upldbtn5']")).click();
				Thread.sleep(4000);
				uploadFile(salesforcelocators.securityDeed);
				Thread.sleep(4000);
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='j_id0:wizId:j_id4:j_id113:j_id166:j_id168:upldbtn6']")).click();
				Thread.sleep(4000);
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@value='Save']")).click();
				Thread.sleep(10000);
				
				uploadFile(salesforcelocators.facilityRequest);
				Thread.sleep(4000);
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:wizId:j_id4:j_id199:j_id215']")).click();
				Thread.sleep(4000);
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:wizId:j_id4:j_id242']")).click();
				Thread.sleep(6000);
				new Select(ThreadLocalWebdriver.getDriver().findElement(By.xpath("//select[@id='j_id0:wizId:j_id4:j_id245:j_id250:j_id251:j_id254']"))).selectByVisibleText("All Investors");
				Thread.sleep(6000);
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@value='Save']")).click();
				Thread.sleep(10000);
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:wizId:j_id4:j_id241']")).click();
				Thread.sleep(4000);
				new Select(ThreadLocalWebdriver.getDriver().findElement(By.xpath("//select[@id='j_id0:wizId:j_id4:j_id264:TimeBasedMPStatus:AmountSectionItem:mpstatus']"))).selectByVisibleText("Now");
				Thread.sleep(6000);
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='j_id0:wizId:j_id4:j_id264:btn2']")).click();
				Thread.sleep(4000);
				ThreadLocalWebdriver.getDriver().switchTo().alert().accept();
				Thread.sleep(8000);
				if(ThreadLocalWebdriver.getDriver().findElement(By.xpath("//div[@id='j_id0:wizId:j_id4:j_id34:j_id35:j_id36:0:j_id37:j_id38:j_id40']")).isDisplayed()) {
				System.out.println("Loan Application Created successfully");	
				
				}else {
					
				System.out.println("Loan application is not created successfully");
				}
				break;
				}
					break;
				}
	pdfResultReport.addStepDetails("Salesforce Approval", "User should able to approve the record successfully",
				"Successfully approve the record from the salesforce application" + " ", "Pass", "Y");
	} catch (Exception e3) {
		log.fatal("Unable to approve the record with Salesforce" + e3.getMessage());
		pdfResultReport.addStepDetails("Salesforce Approval", "User is not able to approve the record from salesforce",
				"Unable to approve the record from salesforce application" + e3.getMessage(), "Fail", "N");
	}
							
			
	
	}
	
	

	
	public void SMEListtoMarketPlace() throws Throwable {
		
		
		try {
		js_type(salesforcelocators.investorSearch, ValidusSmoke_Components.emailIDInvestor,"Search");
		//waitForObj(3000);
		click(salesforcelocators.investorSearchButton);
		String str1=	text(salesforcelocators.salesforceApplications);
		System.out.println(str1);
		String[] ss = str1.split("\n");
		System.out.println(ss.length);
		for(int j=4;j<ss.length;j++) {
			
			if(ss[j].contains("APP-000")) {
				String xx = ss[j];
				System.out.println(xx);
				String[] yy = xx.split(" ");
				String zz = yy[1];
				System.out.println(zz);
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//a[contains(text(),'"+zz+"')]")).click();
				/*Actions a = new Actions(ThreadLocalWebdriver.getDriver());
				a.click(ThreadLocalWebdriver.getDriver().findElement(By.xpath("//*[@id='j_id0:wizId:j_id3']/div[1]/div[4]/a"))).build().perform();
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//*[@id=\"j_id0:wizId:j_id3\"]/div[1]/div[4]/a/text()")).click();*/
				Thread.sleep(10000);   
			//	Screen s = new Screen();
			//	s.click("E:\\SeleniumMavenProject\\SeleniumMavenProject\\MediaFiles\\loan.PNG");
				Thread.sleep(5000);
			//	s.click("E:\\SeleniumMavenProject\\SeleniumMavenProject\\MediaFiles\\Edit.PNG");
				Thread.sleep(5000);
				/*ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='j_id0:j_id33:j_id34:j_id35:j_id37:editBtn']")).click();*/
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='j_id0:j_id33:j_id34:j_id35:j_id37:j_id38:j_id49']")).click();
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@value='Save']")).click();
				Thread.sleep(1000);
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//td[contains(text(),'Loan Documents')]")).click();
				
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:0:j_id95']")).click();
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:1:j_id95']")).click();
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:2:j_id95']")).click();
			/*	ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:3:j_id95']")).click();
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:4:j_id95']")).click();
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:5:j_id95']")).click();
			
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:6:j_id95']")).click();
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:7:j_id95']")).click();
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:8:j_id95']")).click();
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:9:j_id95']")).click();*/
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id93:10:j_id95']")).click();
				
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:j_id54:j_id55:j_id82:j_id92:j_id111']")).click();
				
				ThreadLocalWebdriver.getDriver().findElement(By.xpath("//td[contains(text(),'Promissory Note')]")).click();
				uploadFiles(salesforcelocators.promissoryNote, salesforcelocators.promissoryNoteUpload);
		
		ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='j_id0:wizId:j_id4:j_id60:j_id61:j_id66']")).sendKeys("0.65");
		new Select(ThreadLocalWebdriver.getDriver().findElement(By.xpath("//select[@id='j_id0:wizId:j_id4:j_id60:j_id61:j_id70']"))).selectByVisibleText("3");
		new Select(ThreadLocalWebdriver.getDriver().findElement(By.xpath("//select[@id='j_id0:wizId:j_id4:j_id60:j_id61:j_id76']"))).selectByVisibleText("MONTHLY");
		new Select(ThreadLocalWebdriver.getDriver().findElement(By.xpath("//select[@id='j_id0:wizId:j_id4:j_id60:j_id61:j_id74']"))).selectByVisibleText("Flat");
		ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='j_id0:wizId:j_id4:j_id60:j_id61:j_id84']")).sendKeys("15000");
		ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='j_id0:wizId:j_id4:j_id60:j_id61:j_id88']")).sendKeys("Test");
		
		
		uploadFiles(salesforcelocators.acra_Biz_file, salesforcelocators.acra_Biz_fileUpload);
		Thread.sleep(10000);
		uploadFiles(salesforcelocators.financialDocuments, salesforcelocators.financialDocumentsUpload);
		Thread.sleep(10000);
		uploadFiles(salesforcelocators.paymentRating, salesforcelocators.paymentRatingUpload);
		Thread.sleep(10000);
		uploadFiles(salesforcelocators.noticeRedirect, salesforcelocators.noticeRedirectUpload);
		Thread.sleep(10000);
		uploadFiles(salesforcelocators.securityDeed, salesforcelocators.securityDeedUpload);
		Thread.sleep(10000);
		
		ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@value='Save']")).click();
		Thread.sleep(10000);
		
		uploadFiles(salesforcelocators.facilityRequest, salesforcelocators.facilityRequesttoInvestors);
		//Thread.sleep(10000);
		ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:wizId:j_id4:j_id242']")).click();
		new Select(ThreadLocalWebdriver.getDriver().findElement(By.xpath("//select[@id='j_id0:wizId:j_id4:j_id245:j_id250:j_id251:j_id254']"))).selectByVisibleText("All Investors");
		ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@value='Save']")).click();
		//Thread.sleep(10000);
		ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@name='j_id0:wizId:j_id4:j_id241']")).click();
		//Thread.sleep(2000);
		new Select(ThreadLocalWebdriver.getDriver().findElement(By.xpath("//select[@id='j_id0:wizId:j_id4:j_id264:TimeBasedMPStatus:AmountSectionItem:mpstatus']"))).selectByVisibleText("Now");
		ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='j_id0:wizId:j_id4:j_id264:btn2']")).click();
		//Thread.sleep(2000);
		ThreadLocalWebdriver.getDriver().switchTo().alert().accept();
		if(ThreadLocalWebdriver.getDriver().findElement(By.xpath("//div[@id='j_id0:wizId:j_id4:j_id34:j_id35:j_id36:0:j_id37:j_id38:j_id40']")).isDisplayed()) {
		System.out.println("Loan Application Created successfully");	
		
		}else {
			
		System.out.println("Loan application is not created successfully");
		}
		break;
		}
			break;
		}
		pdfResultReport.addStepDetails("SMEListtoMarketPlace", "User should able to list to a market place successfully",
				"Successfully listed SME into marketplace" + " ", "Pass", "Y");
	} catch (Exception e3) {
		log.fatal("Unable to perform fund a facility request" + e3.getMessage());
		pdfResultReport.addStepDetails("SMEListtoMarketPlace", "User is not able to list to a marketplace",
				"Unable to list SME into marketplace" + e3.getMessage(), "Fail", "N");
	}
	}
		
		public void uploadFile(By loc) {
			try {
				Screen s=new Screen();
				Thread.sleep(2000);
				//s.click("E:\\SeleniumMavenProject\\SeleniumMavenProject\\MediaFiles\\Selectfile1.PNG");
				click(loc);
				Thread.sleep(4000);
				
				s.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/choose.PNG");
				Thread.sleep(2000);
				s.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/testtpdf.png");
				s.click(System.getProperty("user.dir").replace("\\", "/")+"/MediaFiles/openn.png");
				Thread.sleep(4000);	
			}catch(Exception e) {
				System.out.println("Unable to upload the file");
			}
		}
	
		
		public void uploadFiles(By loc, By loc2) {
			try {
		//		Screen s=new Screen();
				//Thread.sleep(2000);
				//s.click("E:\\SeleniumMavenProject\\SeleniumMavenProject\\MediaFiles\\Selectfile1.PNG");
				click(loc);
				Thread.sleep(3000);
		//		s.type("C:\\Automation\\Workspace\\SeleniumAutomation_Validus\\MediaFiles\\Path.PNG", "C:\\Automation\\Workspace\\SeleniumAutomation_Validus\\MediaFiles\\Testt.pdf");
		//		s.click("C:\\Automation\\Workspace\\SeleniumAutomation_Validus\\MediaFiles\\Open.PNG");
				Thread.sleep(3000);
				ThreadLocalWebdriver.getDriver().findElement(loc2).click();
			}catch(Exception e) {
				System.out.println("Unable to upload the file");
			}
		}
	

	public void salesforcefund() throws Throwable {
		try {
			switchwindow(0);
			try {
				switchframe(ThreadLocalWebdriver.getDriver().findElement(By.id("contentPane")));	
				JSClick(salesforcelocators.account, "Account");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				JSClick(salesforcelocators.account, "Account");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		waitForObj(5000);
		
	//	click(By.xpath("//*[@id='bodyCell']/div[3]/div[1]/div/div[2]/table/tbody/tr[2]/th/a"));
		//System.out.println("Locators is:::::"+"//a[contains(text(), 'EswarRao')]");
	//	click(By.xpath("//a[contains(text(), '"+Singapore_VUE_Components.emailIDInvestor1+"')]"));
		//click(By.xpath("//a[contains(text(), 'EswarRao')]"));
		click(By.xpath("//*[@id='bodyCell']/div[3]/div[1]/div/div[2]/table/tbody/tr[2]/th/a"));
		waitForObj(2000);
		mouseOver(salesforcelocators.investorFundTransaction);
		/*Screen n = new Screen();
		n.click("E:\\SeleniumMavenProject\\SeleniumMavenProject\\MediaFiles\\fundtransaction.PNG");*/
		click(salesforcelocators.newFundTransaction);
		waitForObj(3000);
		js_type(salesforcelocators.paymentModeSF, "cash","Cash");
		ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@tabindex='4']")).clear();
		js_type(salesforcelocators.transactionAmount, "500000.00","transactionAmount");
		//set(salesforcelocators.insuredLoanAmount, "20000.00");
	//	set(salesforcelocators.paymentMode, "ACH");
		click(salesforcelocators.savefund);
		waitForObj(6000);
	//	switchbacktodefaultframe();
		pdfResultReport.addStepDetails("salesforcefund", "User should able to fund the application successfully",
				"Successfully funded to the application" + " ", "Pass", "Y");
	} catch (Exception e) {
		log.fatal("Unable to Homelogin" + e.getMessage());
		pdfResultReport.addStepDetails("salesforcefund", "User is not able to fund to the application",
				"Unable to fund  the application" + e.getMessage(), "Fail", "N");
	}
				
	}
	public void salesforceWithDraw() throws Throwable {
		try {
			switchwindow(0);
	//		switchframe(ThreadLocalWebdriver.getDriver().findElement(By.id("contentPane")));	
		click(salesforcelocators.account);
		waitForObj(5000);
	//	click(By.xpath("//*[@id=\"bodyCell\"]/div[3]/div[1]/div/div[2]/table/tbody/tr[2]/th/a"));
		//click(By.xpath("//a[contains(text(), 'EswarRao')]"));
		click(By.xpath("//*[@id='bodyCell']/div[3]/div[1]/div/div[2]/table/tbody/tr[2]/th/a"));
		waitForObj(2000);
		mouseOver(salesforcelocators.investorFundTransaction);
		/*Screen n = new Screen();
		n.click("E:\\SeleniumMavenProject\\SeleniumMavenProject\\MediaFiles\\fundtransaction.PNG");*/
		click(salesforcelocators.newFundTransaction);
		waitForObj(3000);
		js_type(salesforcelocators.paymentModeSF, "cash","Cash");
		ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@tabindex='4']")).clear();
		js_type(salesforcelocators.transactionAmount, "-2000","transactionAmount");
		//set(salesforcelocators.insuredLoanAmount, "20000.00");
	//	set(salesforcelocators.paymentMode, "ACH");
		click(salesforcelocators.savefund);
		waitForObj(4000);
	//	switchbacktodefaultframe();
		pdfResultReport.addStepDetails("salesforceWithDraw", "User should able to WithDraw the amount from application successfully",
				"Successfully funded to the application" + " ", "Pass", "Y");
	} catch (Exception e) {
		log.fatal("salesforceWithDraw" + e.getMessage());
		pdfResultReport.addStepDetails("salesforceWithDraw", "User is not able to WithDraw the amount from application",
				"Unable to fund  the application" + e.getMessage(), "Fail", "N");
	}
				
	}
	
	public void investorfunddetails() throws Exception {
		try {
			switchwindow(0);
			waitForObj(5000);
		ThreadLocalWebdriver.getDriver().navigate().to(ThreadLocalWebdriver.getDriver().getCurrentUrl());
		pdfResultReport.addStepDetails("investorfunddetails", "User should able to see the funded amount on the application successfully",
				"Able to see the funded amount successfully on the application" + " ", "Pass", "Y");
	} catch (Exception e) {
		log.fatal("Unable to Homelogin" + e.getMessage());
		pdfResultReport.addStepDetails("investorfunddetails", "User is not able to see the funded amount on the application",
				"Unable to see the funded amount on the application" + e.getMessage(), "Fail", "N");
	}
	}
	
	public void fundaFacility() {
		try {
			
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("(//a[contains(text(),'Dana')])[1]")).click();
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='investmentAmount']")).sendKeys("1000");
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//a[@id='fundamtclose']")).click();
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//label[@for='filled-in-box']")).click();
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//label[@for='filled-in-box1']")).click();
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//label[@for='filled-in-box2']")).click();
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//label[@for='filled-in-box3']")).click();
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//button[@id='submit']")).click();
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//button[@id='cnf']")).click();
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//a[@href='http://batumbu.com/pinjaman_bahasa_enkripsi/platform/index.php/investor/facilities_participated']")).click();
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//h4[contains(text(),'SEMUA FASILITAS YANG DIDANAI')]")).click();
			/*ThreadLocalWebdriver.getDriver().findElement(By.xpath("//*[@id='myTable4']/tbody/tr/td[1]")).click();
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath("//li[@data-dtr-index='11']")).getText();
			System.out.println(str);*/
			String str1 = ThreadLocalWebdriver.getDriver().findElement(By.xpath("//*[@id='myTable4']/tbody/tr[1]/td[2]")).getText();
			switchwindow(0);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='phSearchInput']")).sendKeys("+str1+");
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='phSearchButton']")).click();
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("(//a[contains(text(),'+str1+')])[1]")).click();
			
			//*[@id="myTable4"]/tbody/tr[1]/td[2]
		}catch(Exception e) {
			
		}
	}
	
	
	public void approvalOTP() throws Throwable{
		
		try {
		Robot r= new Robot();
		 
		 
		 r.keyPress(KeyEvent.VK_CONTROL);
		 r.keyPress(KeyEvent.VK_T);
		 
		 Thread.sleep(3000);
		 switchwindow(1);
		 
		ThreadLocalWebdriver.getDriver().get("https://www.google.com/gmail/about/#");
		click(salesforcelocators.signIN);
		set(salesforcelocators.emailId, "autoqa@validus.sg");
		click(salesforcelocators.emailNext);
		Thread.sleep(3000);
		set(salesforcelocators.gmailPassword,"Validu$Tech");
		click(salesforcelocators.emailNext);
		Thread.sleep(3000);
		set(salesforcelocators.gmailSearch, "sfemails@validus.sg");
		//	ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='gbqfq']")).sendKeys("noreply@salesforce.com");
			//ThreadLocalWebdriver.getDriver().findElement(By.xpath("//td[@style='vertical-align: top;']")).sendKeys("noreply@salesforce.com");
			
			click(salesforcelocators.gmailSearchButton);
		ThreadLocalWebdriver.getDriver().findElement(By.xpath("(//span[contains(text(),'Sandbox: Validus | Login OTP')])[4]")).click();
			
		String strn = ThreadLocalWebdriver.getDriver().findElement(By.xpath("(//div[@class='ii gt'])[1]")).getText();
		System.out.println(strn);
		String otp = null;
		String[] str1 = strn.split("\n");

				System.out.println(str1.length);
				for (int i = 0; i < str1.length; i++) {
					
					if(str1[i].contains("OTP")) {
						String str2 = str1[i];
						String[] str3 = str2.split(":");
						System.out.println(str3.length);
						String otps = str3[0];
						System.out.println(otps);
						
						 otp = otps.trim();
						System.out.println(otp);
						String[] otpno=otp.split(" ");
						System.out.println(otpno.length);
						for (int j = 0; j < otpno.length; j++) {
							System.out.println(otpno[j]);
							
						}
						String otpNum=otpno[4];
						System.out.println(otpNum);
						Thread.sleep(5000);
						
						ThreadLocalWebdriver.getDriver().close();
						
						switchwindow(0);
						
						js_type(salesforcelocators.verifyOtp, otpNum, "OTP");
						click(salesforcelocators.verifySave);
					}}}catch(Exception e) {
						System.out.println(e);
					}
						
			}
	
	public void salesforceLogin() throws Exception {
		try {
			set(salesforcelocators.username, pdfResultReport.testData.get("UserName"));
			set(salesforcelocators.password, pdfResultReport.testData.get("Password"));
			click(salesforcelocators.login);
			pdfResultReport.addStepDetails("Login", "User should login in to the application successfully",
					"Successfully logged in to the application" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("salesforceLogin", "User is not able to login to the application successfully",
					"Unable to login to the application" + e.getMessage(), "Fail", "N");
		}
	}

	public void mailTriggering() throws Throwable {
		
		ThreadLocalWebdriver.getDriver().get("https://www.google.com/gmail/about/#");
		click(salesforcelocators.signIN);
		set(salesforcelocators.emailId, "autoqa@validus.sg");
		click(salesforcelocators.emailNext);
		Thread.sleep(3000);
		set(salesforcelocators.gmailPassword,"Validu$Tech");
		click(salesforcelocators.emailNext);
		Thread.sleep(30000);
		//js_type(By.xpath("//input[@id='gbqfq']"), "noreply@salesforce.com", "search");
		set(salesforcelocators.gmailSearch, "noreply@salesforce.com");
	//	ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='gbqfq']")).sendKeys("noreply@salesforce.com");
		//ThreadLocalWebdriver.getDriver().findElement(By.xpath("//td[@style='vertical-align: top;']")).sendKeys("noreply@salesforce.com");
		
		click(salesforcelocators.gmailSearchButton);
		
	List<WebElement> ele=ThreadLocalWebdriver.getDriver().findElements(By.xpath("//span[text()='Sandbox: Verify your identity in Salesforce']"));
	System.out.println(ele.size());
	for (int i = 0; i < ele.size(); i++) {
		try {
			ele.get(i).click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	//	click(salesforcelocators.gmailMail);
String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath("//div[@id=':ks']")).getText();
System.out.println(str);
String[] str1 = str.split("\n");

		System.out.println(str1.length);
		for (int i = 0; i < str1.length; i++) {
			
			if(str1[i].contains("Verification")) {
				String str2 = str1[i];
				String[] str3 = str2.split(":");
				System.out.println(str3.length);
				String str4 = str3[1];
				System.out.println(str4);
				String str5 =str4.trim();
				System.out.println(str5);
			}
		}
	
	}

	
	
	
	
	public void resetPasswordgmailLink() throws Throwable {
		
		try {
			/*Actions a = new Actions(ThreadLocalWebdriver.getDriver());
			a.keyDown(Keys.CONTROL +"t").build().perform();
			ThreadLocalWebdriver.getDriver().findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"t");*/
	//	Screen s = new Screen();
        Thread.sleep(2000);
	//	s.click("C:\\Users\\user\\Desktop\\Nithin\\E\\Validus\\SeleniumMavenProject\\SeleniumMavenProject\\MediaFiles\\indonesianew.PNG");
		Thread.sleep(4000);
		try {
			switchwindow(1);
			Thread.sleep(1000);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Thread.sleep(4000);
		ThreadLocalWebdriver.getDriver().get("https://www.google.com/gmail/about/#");
		click(salesforcelocators.signIN);
		Thread.sleep(3000);
		set(salesforcelocators.emailId, "autoqa@validus.sg");
		click(salesforcelocators.emailNext);
		Thread.sleep(3000);
		set(salesforcelocators.gmailPassword,"Validu$Tech");
		Thread.sleep(1000);
		click(salesforcelocators.emailNext);
		Thread.sleep(4000);
		/*ThreadLocalWebdriver.getDriver().findElement(By.xpath("//div[@data-tooltip='Settings' and @aria-label='Settings']")).click();
		Thread.sleep(2000);
		ThreadLocalWebdriver.getDriver().findElement(By.xpath("//div[contains(text(),'Go back to classic Validus Capital Mail')]")).click();
		Thread.sleep(5000);*/
		//js_type(By.xpath("//input[@id='gbqfq']"), "noreply@salesforce.com", "search");
		set(salesforcelocators.gmailSearch, "Batumbu Admin");
	//	ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='gbqfq']")).sendKeys("noreply@salesforce.com");
		//ThreadLocalWebdriver.getDriver().findElement(By.xpath("//td[@style='vertical-align: top;']")).sendKeys("noreply@salesforce.com");
		
		try {
			click(salesforcelocators.gmailSearchButton);
			Thread.sleep(3000);
			List <WebElement> strr = ThreadLocalWebdriver.getDriver().findElements(By.xpath("//b[contains(text(),'Sandbox: Reset Password')]"));
			strr.size();
			System.out.println(strr.size());
			for(int j=0;j<strr.size();j++) {
				if(strr.get(j).isDisplayed()) {
				strr.get(j).click();
			}
				else {
					continue;
				}
		} }catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath("//div[@class='gs']")).getText();
		String stp = null;
		System.out.println(str);
		String[] st  = str.split("\n");
		for(int i = 0; i< st.length; i++)
		{
			
			if(st[i].contains("Temparory ")) {
				
				String stn = st[i];
				String[] stm = stn.split(" ");
				System.out.println(stm.length);
				String sto = stm[5];
				System.out.println(sto);
				 stp = sto.trim();
				System.out.println(stp);
			}
		}		
				
		ThreadLocalWebdriver.getDriver().findElement(By.xpath("(//span[contains(text(),'batumbu')])[2]")).click();
		Thread.sleep(3000);
		
		switchwindow(2);
		set(By.xpath("//input[@id='Password']"), stp);
		click(registerasInvestorlocators.resetpasswordbuttonfinal);
		waitForObj(3000);
		set(registerasInvestorlocators.resetpasswordfinal, pdfResultReport.testData.get("ResetPassword"));
		set(registerasInvestorlocators.resetConfirmPasswordFinal, pdfResultReport.testData.get("ResetConfirmPassword"));
		click(registerasInvestorlocators.resetpasswordbuttonfinal);
		
		String snn = ThreadLocalWebdriver.getDriver().findElement(By.xpath("//div[@class='alert alert-success']")).getText();
		System.out.println(snn);
		
		js_type(validussmokelocators.emailIDInvestor, ValidusSmoke_Components.emailIDInvestor, "username");
		set(validussmokelocators.passwordInvestor, pdfResultReport.testData.get("ResetPassword"));
		click(registerasInvestorlocators.login);
		
		
		
			pdfResultReport.addStepDetails("resetPasswordloginscreen", "User should able to login after the reset link  successfully",
					"Successfully user is able to login with resetpassword link successfully" + " ", "Pass", "Y");
		}catch (Exception e3) {
			log.fatal("Unable to login with reset password " + e3.getMessage());
			pdfResultReport.addStepDetails("resetPasswordLogin", "User is not able to login after the reset password link",
					"Unable to login after the reset password successfully" + e3.getMessage(), "Fail", "N");
		}
		
		}
		
		public void resetPasswordOTP() throws Exception {
			try {
		
		switchwindow(1);
		Thread.sleep(3000);
	//	Screen s = new Screen();
	//	s.click("C:\\Users\\user\\Desktop\\Nithin\\E\\Validus\\SeleniumMavenProject\\SeleniumMavenProject\\MediaFiles\\Delete.PNG");
		Thread.sleep(2000);
		ThreadLocalWebdriver.getDriver().findElement(By.xpath("(//span[@class='nU n1'])[1]")).click();
		Actions a = new Actions(ThreadLocalWebdriver.getDriver());
		a.sendKeys(Keys.F5).build().perform();
		
		ThreadLocalWebdriver.getDriver().navigate().to(ThreadLocalWebdriver.getDriver().getCurrentUrl());
		Thread.sleep(5000);
		ThreadLocalWebdriver.getDriver().findElement(By.xpath("(//div[@class='asa'])[6]")).click();
		
	/*	try {
			List <WebElement> san = ThreadLocalWebdriver.getDriver().findElements(By.xpath("//b[contains(text(),'Sandbox: Validus | Login OTP')]"));
			san.size();
			System.out.println(san.size());
			for(int j=0;j<san.size();j++) {
				if(san.get(j).isDisplayed()) {
					san.get(j).click();
			}
				else {
					continue;
				}
		} }catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		ThreadLocalWebdriver.getDriver().findElement(By.xpath("//b[contains(text(),'Sandbox: Validus | Login OTP')]")).click();
		
		String nn = ThreadLocalWebdriver.getDriver().findElement(By.xpath("(//div[@class='ii gt'])[1]")).getText();
		String nithin = null;
		System.out.println(nn);
		String[] ni = nn.split("\n");
		for(int i = 0; i<ni.length; i++) {
			
			if(ni[i].contains("OTP")) {
				
				String nit = ni[i];
				String[] nith = nit.split(" ");
				System.out.println(nith.length);
				String nithi = nith[4];
				System.out.println(nithi);
				 nithin = nithi.trim();
				System.out.println(nithin);
				
			}else {
				continue;
			}
		}
		//	s.click("C:\\Users\\user\\Desktop\\Nithin\\E\\Validus\\SeleniumMavenProject\\SeleniumMavenProject\\MediaFiles\\Delete.PNG");
			
		
			switchwindow(2);
			set(By.xpath("//input[@id='ip_otp']"), nithin);
			click(validussmokelocators.otpSubmit);
			
			waitForObj(5000);
			
		
		
	
		pdfResultReport.addStepDetails("resetPasswordgmailotp", "User should able to login after resetpasswordotp successfully",
				"Successfully user is able to login after resetpasswordotp successfully" + " ", "Pass", "Y");
	} catch (Exception e3) {
		log.fatal("Unable to login after resetpasswordotp " + e3.getMessage());
		pdfResultReport.addStepDetails("resetPasswordgmailotp", "User is not able to login after the resetpasswordotp successfully",
				"Unable to login after reset password " + e3.getMessage(), "Fail", "N");
	
	}
	
			
			
	
		}
		}
