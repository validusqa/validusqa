package com.objectRepository;

import org.openqa.selenium.By;

public class Batumbu_AdminPortal_Loc {

	
	public By username=By.xpath("//input[@name='username']");
	public By password=By.xpath("//input[@name='password']");
	public By login=By.xpath("//div//button[text()='Login']");
	
	public By smes=By.xpath("//html/body/div[2]/aside[1]/section/ul/li[2]/a/span[1]");
	public By search=By.xpath("//input[@type=\"search\"]");
	public By viewDetails=By.xpath("//table[@id=\"example1\"]//tr//td/a[text()='View Details']");
	
	
}
