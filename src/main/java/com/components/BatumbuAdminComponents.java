package com.components;

import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

import com.baseClasses.BaseClass_Web;
import com.baseClasses.PDFResultReport;
import com.baseClasses.ThreadLocalWebdriver;
import com.objectRepository.Batumbu_AdminPortal_Loc;

public class BatumbuAdminComponents extends BaseClass_Web{
	
	public Batumbu_AdminPortal_Loc batumbu_AdminPortal_Loc=new Batumbu_AdminPortal_Loc();

	public BatumbuAdminComponents(PDFResultReport pdfresultReport){
		this.pdfResultReport=pdfresultReport;
	}
	
	public void openURL() throws Exception{  
		try{
			//launchapp(pdfResultReport.testDataValue.get("AppURL"));
			launchapp("https://batumbu.id/BatumbuAdmin/public/");
			pdfResultReport.addStepDetails("openURL","Application should open the url","Successfully opened the URL" + " ","Pass", "Y");
		  }catch(Exception e){
			  log.fatal("Unable to open the URL"+e.getMessage());
			  pdfResultReport.addStepDetails("openURL","Application should open the url", "Unable to open the URL"+e.getMessage(),"Fail", "N");
	  }
		
	}	
	
	public void adminLogin() throws Throwable {
		try {
			js_type(batumbu_AdminPortal_Loc.username, "portaladmin@batumbu.id", "username");
			set(batumbu_AdminPortal_Loc.password, "Admin123");
			click(batumbu_AdminPortal_Loc.login);
			waitForObj(3000);
			waitForElement(batumbu_AdminPortal_Loc.smes, 15);
			pdfResultReport.addStepDetails("adminLogin","Application should successfully login to the application for reset password","successfully logged in to the application for  reset password" + " ","Pass", "Y");	
		 }catch(Exception e){
			  log.fatal("Unable to open the URL"+e.getMessage());
			  pdfResultReport.addStepDetails("adminLogin","Not able to login to the application for reset a passwordn", "Unable to login to the application for reset a passwordn"+e.getMessage(),"Fail", "N");
	  }
	}
	
	public void navigateToSMESearch() throws Throwable {
		try {
			click(batumbu_AdminPortal_Loc.smes);
			Thread.sleep(2000);
			waitForElement(batumbu_AdminPortal_Loc.search, 15);
			Thread.sleep(2000);
			js_type(batumbu_AdminPortal_Loc.search, "nag0305034241@gmailx.com","Search SME");
			Thread.sleep(2000);
			click(batumbu_AdminPortal_Loc.search);
			Actions a=new Actions(ThreadLocalWebdriver.getDriver());
			a.sendKeys(Keys.END).build().perform();
			Robot r=new Robot();
			r.keyPress(KeyEvent.VK_END);
			r.keyRelease(KeyEvent.VK_END);
			Thread.sleep(2000);
			System.out.println("Clicked on End");
			click(batumbu_AdminPortal_Loc.viewDetails);
			waitForObj(3000);
			pdfResultReport.addStepDetails("adminLogin","Application should successfully login to the application for reset password","successfully logged in to the application for  reset password" + " ","Pass", "Y");	
		 }catch(Exception e){
			  log.fatal("Unable to open the URL"+e.getMessage());
			  pdfResultReport.addStepDetails("adminLogin","Not able to login to the application for reset a passwordn", "Unable to login to the application for reset a passwordn"+e.getMessage(),"Fail", "N");
	  }
	}
	
}