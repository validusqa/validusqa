package com.testScripts_Singapore;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.baseClasses.BaseClass_Web;
import com.components.Singapore_SME_PHP_Components;
import com.components.Singapore_VUE_Components;
import com.components.Singapore_VUE_SalesforceComponents;
import com.components.ValidusRegisterAsInvestorComponents;
import com.components.ValidusSalesforceComponents;
import com.components.ValidusSmoke_Components;

public class Singapore_SME_LLC_PurchaseOrderFinancing extends BaseClass_Web{
	public Singapore_VUE_Components singapore_VUE_Components = new Singapore_VUE_Components(pdfResultReport);
	public ValidusSalesforceComponents salesforceComponent = new ValidusSalesforceComponents(pdfResultReport);
	public Singapore_VUE_SalesforceComponents singapore_VUE_SalesforceComponents = new Singapore_VUE_SalesforceComponents(pdfResultReport);
	public Singapore_SME_PHP_Components singapore_SME_PHP_Components =new Singapore_SME_PHP_Components(pdfResultReport); 
	public void initializeRepository() throws Exception {		
		reportDetails.put("Test Script Name", this.getClass().getSimpleName());
		reportDetails.put("Test Script MyWorksshop Document ID", "Doc1234567");
		reportDetails.put("Test Script Revision No", "1");
		reportDetails.put("Test Author Name", "Nithin");
		reportDetails.put("Test Script Type", "User Acceptance Testing");
		reportDetails.put("Requirement Document ID of System", "Doc1234567");
		reportDetails.put("Requirement ID", "US2202");
	}
	
	@Parameters("TestcaseNo")
	@Test(description = "Scenario:1 - Test the SME functionality in Singapore platform")
  public void f(String no) throws Throwable {
	  System.out.println("Entered in the test method..................");
	  try {
		//  System.out.println(System.getProperty("user.dir").replace("\\", "/")	+ "/Resources/Validus_Testdata.xlsx");
		pdfResultReport.readTestDataFile(System.getProperty("user.dir").replace("\\", "/")	+ "/Resources/Singapore_Testdata.xls", no);
	} catch (Exception e) {
		System.out.println("Unable to read the data from excel file");
	}
	  initializeRepository();
	  singapore_SME_PHP_Components.openURL();
	  singapore_VUE_Components.CreateAccount_SMENEW();
  	  singapore_VUE_SalesforceComponents.singaporeSalesforceIntegrationLogin();
  	  singapore_VUE_SalesforceComponents.singaporeSalesforceOTP();
	  singapore_VUE_Components.login();
	  singapore_SME_PHP_Components.registeringasSME_LLC("LLC");
	  singapore_SME_PHP_Components.bankDetails_SME();
	  singapore_SME_PHP_Components.application();
	  singapore_SME_PHP_Components.purchaseOrderFinancingSME();
	  salesforceComponent.salesforceApprovalSME();
	  salesforceComponent.approveLoan_SME2();
/*	  singapore_SME_PHP_Components.login();
	  singapore_SME_PHP_Components.accountSummary();
	  singapore_SME_PHP_Components.allLiveFecilities_Fund();
	  singapore_SME_PHP_Components.allLiveFecilities_Confirm();
	  singapore_SME_PHP_Components.accountSummary();
	  singapore_SME_PHP_Components.withdrawal();
	  singapore_SME_PHP_Components.withdrawalConfirm();
	  singapore_SME_PHP_Components.accountSummary();*/
	//  investorComponents.logout();
	  
	}
}
