package com.objectRepository;

import org.openqa.selenium.By;

public class Validus_SalesforceLoc {

	
	public By username = By.xpath("//input[@id='username']");
	public By password = By.xpath("//input[@id='password']");
	public By login = By.xpath("//input[@id='Login']");
	public By unregisteredUsers = By.xpath("//a[contains(text(),'Unregistered Users')]");
    public By go = By.xpath("//input[@name='go']");
    
    public By signIN = By.xpath("//div[@class='gmail-nav__nav-links-wrap']/a[text()='Sign In']");
    public By signIN2 = By.xpath("//html/body/div[2]/div[1]/div[5]/ul[1]/li[2]/a");
    
    
    public By emailId = By.xpath("//input[@id='identifierId']");
    public By emailNext = By.xpath("//span[contains(text(),'Next')]");
    public By gmailPassword = By.xpath("//input[@name='password']");
    public By gmailSearch=By.xpath("//input[@aria-label='Search mail']");
    public By gmailSearchButton = By.xpath("//button[@aria-label='Search Mail']");
    public By gmailMail = By.xpath("(//span[text()='Sandbox: Verify your identity in Salesforce'])[1]");
    public By gmailDelete = By.xpath("//*[@id=\":5\"]/div[2]/div[1]/div/div[2]/div[3]/div");
	public By saisaUsername = By.xpath("//input[@id='ctl00_MPH_txtUserName']");
	public By saisaPassword = By.xpath("//input[@id='ctl00_MPH_txtPassword']");
    public By mailLogin = By.xpath("//a[@id='btnLogin']");
    public By lastButton = By.xpath("//img[@class='last']");
    public By verifyOtp = By.xpath("//input[@id='ip_otp']");
    public By verifySave = By.xpath("//button[@type='submit']");
    public By otpinvestor = By.xpath("//input[@id='otpEntered']");
    public By continueInvestor = By.xpath("//a[@value='Submit']");
    
    public By home = By.xpath("//a[@title='Home Tab']");
    public By searchText = By.xpath("//input[@title='Search...']");
    
    public By searchButton = By.xpath("//input[@id='phSearchButton']");
    public By vikasNahata = By.xpath("//a[contains(text(),'Vikas Nahata')]");
    public By dropDown = By.xpath("//a[@id='moderatorMutton']");
    public By userDetails = By.xpath("//a[@id='USER_DETAIL']");
    public By loginVikasNahata = By.xpath("//input[@title='Login']");
    public By secondSearch = By.xpath("//input[@id='secondSearchButton']");
  //  public By investorSearch = By.xpath("//div[@class='uiInput uiAutocomplete uiInput--default']");
    public By investorSearch = By.xpath("//input[@id='phSearchInput']");
    public By secondSearchText = By.xpath("//input[@id='secondSearchText']"); 
    public By investorSearchButton = By.xpath("//input[@id='phSearchButton']");
    public By secondSearchButton = By.xpath("//input[@id='secondSearchButton']");
    public By unRegisteredUSername = By.xpath("//a[@class='slds-truncate outputLookupLink slds-truncate forceOutputLookup']");
    public By salesforceApplications = By.xpath("//div[@id='genesis__Applications__c']");
    public By recordlist = By.xpath("//table[@class='list']");
    public By otp = By.xpath("//input[@placeholder='Please enter OTP']");
    public By verify = By.xpath("//button[contains(text(), 'Verify')]");
    public By emailVerificationLink = By.xpath("//a[contains(text(),'http://stageplatform.validus.sg/#/confirm?id=a7...')]");
    public By loginNow = By.xpath("//button[contains(text(), 'Login Now')]");
    public By creditDocuments = By.xpath("//td[text()='Credit Documents']");
    public By cdEdit = By.xpath("(//input[@value='Edit'])[2]");
    public By verifiedDate1 = By.xpath("//input[@id='j_id0:j_id196:j_id197:frm:j_id217:j_id225:0:j_id229']");
    public By verifiedDate2 = By.xpath("//input[@id='j_id0:j_id196:j_id197:frm:j_id217:j_id225:1:j_id229']");
    public By verifiedDate3 = By.xpath("//input[@id='j_id0:j_id196:j_id197:frm:j_id217:j_id225:2:j_id229']");
    public By verifiedDate4 = By.xpath("//input[@id='j_id0:j_id196:j_id197:frm:j_id217:j_id225:3:j_id229']");
    public By verifiedDate5 = By.xpath("//input[@id='j_id0:j_id196:j_id197:frm:j_id217:j_id225:4:j_id229']");
    public By approve1 = By.xpath("//input[@id='j_id0:j_id196:j_id197:frm:j_id217:j_id225:0:j_id235']");
    public By approve2 = By.xpath("//input[@id='j_id0:j_id196:j_id197:frm:j_id217:j_id225:1:j_id235']");
    public By approve3 = By.xpath("//input[@id='j_id0:j_id196:j_id197:frm:j_id217:j_id225:2:j_id235']");
    public By approve4 = By.xpath("//input[@id='j_id0:j_id196:j_id197:frm:j_id217:j_id225:3:j_id235']");
    public By approve5 = By.xpath("//input[@id='j_id0:j_id196:j_id197:frm:j_id217:j_id225:4:j_id235']");
    public By cdSave = By.xpath("//input[@value='Save']");
    public By KYC = By.xpath("//td[text()='KYC']");
    public By KYCUpload = By.xpath("//input[@id='j_id0:j_id257:j_id258:frm:j_id286:j_id287:j_id288:upldbtn1']");
    public By KYCApprove = By.xpath("//input[@value='Approve']");
    public By loanApplication = By.xpath("//*[@id='j_id0:wizId:j_id3']/div[1]/div[4]/a");
    public By LAEdit = By.xpath("//input[@id='j_id0:j_id33:j_id34:j_id35:j_id37:editBtn']");
    public By checkMark = By.xpath("//input[@id='j_id0:j_id33:j_id34:j_id35:j_id37:j_id38:j_id50']");
    public By LDSave = By.xpath("//input[@value='Save']");
    public By LoanDocuments = By.xpath("//td[contains(text(),'Loan Documents')]");
    public By LDEdit = By.xpath("(//input[@value='Edit'])[2]");
    public By VerifyAll = By.xpath("//input[@name='j_id0:j_id58:j_id59:j_id86:j_id103:j_id104:j_id107']");
    public By vfSave = By.xpath("(//input[@value='Save'])[2]");
    public By promissoryNote = By.xpath("//*[@id='j_id0:BorrowerPromisoryDocsTab_lbl']");
    public By promissoryNoteUpload = By.xpath("(//table[@class='detailList']//input[@value='Upload'])[2]");
    public By acra_Biz_file = By.xpath("//input[@id='j_id0:wizId:j_id4:j_id113:j_id114:j_id116:j_id117']");
    public By acra_Biz_fileUpload = By.xpath("//input[@id='j_id0:wizId:j_id4:j_id113:j_id114:j_id116:upldbtn1']");
    public By financialDocuments = By.xpath("//input[@id='j_id0:wizId:j_id4:j_id113:j_id127:j_id129:j_id130']");
    public By financialDocumentsUpload = By.xpath("//input[@id='j_id0:wizId:j_id4:j_id113:j_id127:j_id129:upldbtn2']");
    public By paymentRating = By.xpath("//input[@id='j_id0:wizId:j_id4:j_id113:j_id140:j_id142:j_id143']");
    public By paymentRatingUpload = By.xpath("//input[@id='j_id0:wizId:j_id4:j_id113:j_id140:j_id142:upldbtn3']");
    public By noticeRedirect = By.xpath("//input[@id='j_id0:wizId:j_id4:j_id113:j_id153:j_id155:j_id156']");
    public By noticeRedirectUpload = By.xpath("//input[@id='j_id0:wizId:j_id4:j_id113:j_id153:j_id155:upldbtn5']");
    public By securityDeed = By.xpath("//input[@id='j_id0:wizId:j_id4:j_id113:j_id166:j_id168:j_id169']");
    public By securityDeedUpload = By.xpath("//input[@id='j_id0:wizId:j_id4:j_id113:j_id166:j_id168:upldbtn6']");
    public By facilityRequest = By.xpath("//input[@id='j_id0:wizId:j_id4:j_id199:j_id226:j_id227:0:j_id233']");
    public By facilityRequesttoInvestors = By.xpath("//input[@name='j_id0:wizId:j_id4:j_id199:j_id215']");
    
    public By documentCheck = By.xpath("//div[@class='completed-step']//a[text()=' Documents Check']");
    public By proceedToUploadCSV = By.xpath("//input[@value='Proceed to Upload CSV']");
    public By ChooseFile = By.xpath("//input[@id='j_id0:wizId:j_id4:inst:j_id244']");
    public By importVoice = By.xpath("//input[@value='Import Invoice']");
    public By documentCheckSave = By.xpath("//input[@value='Save']");
    public By documentCheckApprove = By.xpath("//input[@value='Approve Invoice']");
    
    public By approveLoan = By.xpath("//a[text()=' Approve Loan']");
    public By approveLoanEdit = By.xpath("//input[@value='Edit']");
    public By interestRate = By.xpath("//input[@id='j_id0:wizId:j_id4:j_id60:j_id61:j_id66']");
    public By invoiceAmount = By.xpath("//input[@id='j_id0:wizId:j_id4:j_id60:j_id61:j_id92']");
    public By nameofSellers = By.xpath("//input[@id='j_id0:wizId:j_id4:j_id60:j_id61:j_id96']");
    public By tenureOffered = By.xpath("//select[@id='j_id0:wizId:j_id4:j_id60:j_id61:j_id77']");
    public By tenureOffered1 = By.xpath("//select[@id='j_id0:wizId:j_id4:j_id60:j_id61:j_id76']");
    public By tenureOffered2 = By.xpath("//select[@id='j_id0:wizId:j_id4:j_id60:j_id61:j_id79']");
    public By tenureOffered3 = By.xpath("//select[@id='j_id0:wizId:j_id4:j_id60:j_id61:j_id80']");
    public By paymentFrequency = By.xpath("//select[@id='j_id0:wizId:j_id4:j_id60:j_id61:j_id84']");
    public By interestType = By.xpath("//select[@id='j_id0:wizId:j_id4:j_id60:j_id61:j_id82']");
    public By insurenceFee = By.xpath("//input[@id='j_id0:wizId:j_id4:one:j_id110:j_id123']");
    public By approveloanSave = By.xpath("//input[@value='Save']");
    public By FacilityRequest = By.xpath("//input[@value='Facility Request to Investors']");
    public By approveLoanApprove = By.xpath("//input[@value='Approve']");
    public By investerType = By.xpath("//select[@id='j_id0:wizId:j_id4:j_id277:j_id296:j_id297:j_id300']");
    public By investerTypeSave = By.xpath("//input[@value='Save']");
    public By listtoMarketPlace = By.xpath("//div[@align='center']//span/input[@value='List to MarketPlace']");
    public By marketPlaceStatus = By.xpath("//select[@id='j_id0:wizId:j_id4:j_id324:TimeBasedMPStatus:AmountSectionItem:mpstatus']");
    public By marketPlaceSave = By.xpath("//input[@value='Save']");
    
    public By applications = By.xpath("//a[@class='brandPrimaryFgr']");
    public By application = By.xpath("//a[@title='Applications Tab']");
    public By listApplication = By.xpath("//div[@class='listRelatedObject Custom27Block']");
    public By investorDocument = By.xpath("//a[contains(text(),'Investor Documents')]");
    public By investorapplication = By.xpath("(//span[contains(text(),'2')])[4]");
    public By Investordocument = By.xpath("(//span[@class='label'])[2]");
    public By singaporeApproveLink = By.xpath("//form[@id='j_id0:wizId:j_id3']//div[@class='completed-step']//a[text()=' Approve Application']");
    public By singaporeUploadLink = By.xpath("//table[@class='detailList']//input[@value='Upload']");
    public By singaporeUploadLink_SME = By.xpath("(//table[@class='detailList']//input[@value='Upload'])[2]");
    public By singaporeApproved = By.xpath("//div[@align='center']//input[@value='Approve']");
    
    public By approveApplications = By.xpath("(//span[contains(text(),'4')])[2]");
  //  public By approveApplications = By.xpath("//*[@id='j_id0:wizId:j_id3']/div[1]/div[4]/a");
    public By chooseFile = By.xpath("//input[@name='j_id0:wizId:j_id3:j_id59:j_id60:j_id61:j_id62:inputFile:file']");
    public By file = By.xpath("//input[@name='j_id0:wizId:j_id3:j_id59:j_id60:j_id61:upldbtn1']");
    public By approveInvestor = By.xpath("//input[@id='j_id0:wizId:j_id3:j_id72:approveBtn']");
    public By rejectInvestor = By.xpath("//input[@name='j_id0:wizId:j_id3:j_id72:rejectBtn']");
    public By statusInvestor = By.xpath("//span[@style='text-align:center']");
 
    public By choosefile = By.xpath("//input[@id='j_id0:wizId:j_id3:j_id59:j_id60:j_id61:j_id62']");
    public By logout = By.xpath("//a[@href='http://batumbu.com/pinjaman_bahasa_enkripsi/platform/index.php/home/logout']");
    public By account = By.xpath("(//a[contains(text(),'Accounts')])[1]");
    public By paymentMode = By.xpath("//input[@tabindex='3']");
    public By investorFundTransaction = By.xpath("(//span[contains(text(),'Investor Fund Transaction')])[1]");
    public By newFundTransaction = By.xpath("(//input[@title='New Investor Fund Transaction'])[1]");
    public By paymentModeSF = By.xpath("//*[@id='CF00N28000005zwx3']");
   
    public By transactionAmount = By.xpath("//input[@tabindex='4']");
    public By insuredLoanAmount = By.xpath("//input[@id='00N0K00000JEILD']");
    public By savefund = By.xpath("(//input[@name='save'])[1]");
    public By fundsavailable = By.xpath("//div[@title='Funds available for investing']");
}

