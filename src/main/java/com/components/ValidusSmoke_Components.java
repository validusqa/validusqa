package com.components;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.openqa.selenium.By;
import com.baseClasses.BaseClass_Web;
import com.baseClasses.PDFResultReport;
import com.objectRepository.ValidusRegisterAsInvester;
import com.objectRepository.ValidusSmoke_Loc;


public class ValidusSmoke_Components extends BaseClass_Web {

	public ValidusSmoke_Loc validussmokelocators = new ValidusSmoke_Loc();
	public ValidusRegisterAsInvester registerasInvestorlocators=new ValidusRegisterAsInvester();
	public static String emailIDInvestor;
	public static String emailIDInvestor1;
	public ValidusSmoke_Components(PDFResultReport pdfresultReport) {
		this.pdfResultReport = pdfresultReport;
	}

	public void openURL() throws Exception {
		try {
			launchapp(pdfResultReport.testDataValue.get("AppURL"));
			pdfResultReport.addStepDetails("openURL", "Application should open the url",
					"Successfully opened the URL" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to open the URL" + e.getMessage());
			pdfResultReport.addStepDetails("openURL", "Application should open the url",
					"Unable to open the URL" + e.getMessage(), "Fail", "N");
		}

	}

	public void Homelogin() throws Exception {

		try {
			click(validussmokelocators.validusMainpageLogin);
			pdfResultReport.addStepDetails("Homelogin", "Application should accept the credentials",
					"Successfully login to the home" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("Homelogin", "Unable to enter the credentials",
					"Unable to Homelogin" + e.getMessage(), "Fail", "N");
		}
	}

	public void CreateAccount() throws Throwable {
		try {
		//	click(validussmokelocators.validusMainpageLogin);
			click(validussmokelocators.validusCreateanAccount);
			waitForElement(validussmokelocators.validusInvestor, 30);
			waitForObj(3000);
			click(validussmokelocators.validusInvestor);
			
			waitForElement(validussmokelocators.Setuju2_Investor, 30);
			waitForObj(2000);
			js_type(validussmokelocators.Setuju2_Investor_Textbx, "1000000000", "Enter Amt");
			
			click(validussmokelocators.Setuju2_Investor);
			waitForObj(2000);
			
			SimpleDateFormat df = new SimpleDateFormat("MMddhhmmss");
			Date d = new Date();
			String time = df.format(d);
			System.out.println("time::" + time);
			emailIDInvestor1="nag"+time;
			System.out.println("emailIDInvestor1 ::"+emailIDInvestor1);
			emailIDInvestor = emailIDInvestor1 + "@gmailx.com";
			System.out.println("emailIDInvestor :::"+emailIDInvestor);
			//set(valsidussmokelocators.emailIDInvestor, emailIDInvestor);
		//	set(validussmokelocators.emailIDInvestor, pdfResultReport.testData.get("EmailIDInvestor"));
			js_type(validussmokelocators.emailIDInvestor, emailIDInvestor, "EmailId");
			js_type(validussmokelocators.passwordInvestor, pdfResultReport.testData.get("PasswordInvestor"), "Password");
			js_type(validussmokelocators.confirmPasswordInvestor, pdfResultReport.testData.get("ConfirmPasswordInvestor"), "Confirm Password");
			SimpleDateFormat df1 = new SimpleDateFormat("hhmmss");
			Date d1 = new Date();
			String time1 = df1.format(d1);
			String time2 = "+621"+time1;
			System.out.println("time2::" + time2);
		
			Thread.sleep(2000);
			js_type(validussmokelocators.mobileNumber, time2,"mobilenumber");
			click(validussmokelocators.investorLoginTermsandCondition);
			Thread.sleep(2000);
			click(registerasInvestorlocators.Secondcheckbox);
			click(registerasInvestorlocators.esignatureAgree);
			Thread.sleep(2000);
			pdfResultReport.addStepDetails("Create account", "Application should allow the user to enter details",
					"Successfully created an account" + " ", "Pass", "Y");
			click(validussmokelocators.investorContinue);
			waitForElement(validussmokelocators.otpContent, "30");
			text(validussmokelocators.otpContent);
			waitForObj(3000);
			
			//ThreadLocalWebdriver.getDriver().quit();
		} catch (Exception e) {
			System.out.println(e);
			log.fatal("Unable to create account" + e.getMessage() + "Line Number :" + e.getStackTrace());
			pdfResultReport.addStepDetails("Create account", "Unable to enter details",
					"Unable to create account" + e.getMessage(), "Fail", "N");

		}
	}

	private void waitForElement(By otpContent, String string) {
		// TODO Auto-generated method stub
		
	}

	public void investoraccount() {

		try {

		} catch (Exception e) {

		}
	}

}
