package com.baseClasses;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class BrowserFactory {
	public static String browser;
	
/*	BrowserFactory(String name){
		browser=name;
	}*/
	
	 static WebDriver openBrowser(String browserName) {
	        WebDriver driver = null;
	        if (browserName.toLowerCase().contains("firefox")) {
	            driver = new FirefoxDriver();
	            browser="Firefox";
	            return driver;
	        }
	        if (browserName.toLowerCase().contains("internet")) {
	            driver = new InternetExplorerDriver();
	            browser="IE";
	            return driver;
	        }
	        if (browserName.toLowerCase().contains("windowschrome")) {
	        //	System.setProperty("webdriver.chrome.driver", "/home/automation/Bitbucket/testing/linux/chromedriver");
	        	//home/ubuntu/Downloads/SeleniumAutomation_Validus/lib
	        //	System.out.println( System.getProperty("user.dir").replace("\\", "/")	+"/linux/chromedriver");
	        	System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir").replace("\\", "/")	+"/Lib/chromedriver.exe");
	            driver = new ChromeDriver();	
	            browser="Google Chrome";
	            System.out.println("browser   :::::::"+browser);
		        driver.manage().window().maximize();
		        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	            return driver;
	        }
	        if (browserName.toLowerCase().contains("linuxchrome")) {
		        	System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir").replace("\\", "/")	+"/linux/chromedriver");
		            driver = new ChromeDriver();	
		            browser="Google Chrome";
		            System.out.println("browser   :::::::"+browser);
			        driver.manage().window().maximize();
			        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		            return driver;
		        }

	        return driver;
	    }
	}
