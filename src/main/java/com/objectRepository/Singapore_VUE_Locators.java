package com.objectRepository;

import org.openqa.selenium.By;

public class Singapore_VUE_Locators {

	// VUE Locators Create an Account
	public By registerNow = By.xpath("//*[@id='app']/div/section/main/div/div/div/div/div/form/div[5]/div/a");
	
	public By register = By.xpath("//*[@id='app']/div/div[2]/div[2]/div/div[1]/div/ul/li[1]/span");
	public By sme = By.xpath("(//span[@class=\"checkmark\"])[1]");
	public By investor = By.xpath("(//span[@class=\"checkmark\"])[2]");
	public By acknowledgement = By.xpath("(//span[@class=\"checkmark\"])[3]");
	public By emailNew = By.xpath("//input[@placeholder='This will be your username']");
	public By mobileNoNew=By.xpath("//input[@placeholder='Please enter your mobile number']");
	public By passwordNew = By.xpath("//input[@placeholder='At least 8 characters, case sensitive']");
	public By confirmPasswordNew = By.xpath("//input[@placeholder='Confirm your password']");
	public By continueBtnNew=By.xpath("//button[contains(text(), 'Create free account')]");
	
	public By email = By.xpath("//*[@id=\"pane-step1\"]/div/form/div[1]/div/div/input");
	public By password = By.xpath("(//input[@type=\"password\"])[1]");
	
	public By confirmPassword = By.xpath("(//input[@type=\"password\"])[2]");
	public By mobileNo=By.xpath("(//input[@type='text'])[3]");
	public By checkBX=By.xpath("//*[@id='pane-step1']/div/form/div[5]/div/label/span/span");
	public By continueBtn=By.xpath("//button[@class='el-button cta el-button--primary']");
	
	
	
	//Login Page
	
	public By loginMail=By.xpath("//*[@id='app']/div/div[2]/div[2]/div/div[2]/div/div[1]/div[2]/input");
	public By loginPassword=By.xpath("//input[@name=\"val.password\"]");
	public By loginCheckBX=By.xpath("//*[@id=\"app\"]/div/section/main/div/div/div/div/div/form/div[2]/div/div/div/label/span/span");
	public By login = By.xpath("//div[@class='login-button-container']//button[contains(text(), 'Login')]");
	
	//Register as Investor Page
	
	public By individualInvestorSingaporeCitizen=By.xpath("(//span[@class='el-radio__inner'])[1]");
	public By individualInvestorNONSingaporeCitizen=By.xpath("(//span[@class='el-radio__inner'])[2]");
	public By companyIncorporatedInSingapore=By.xpath("(//span[@class='el-radio__inner'])[3]");
	public By companyIncorporatedOutsideSingapore=By.xpath("(//span[@class='el-radio__inner'])[4]");
	
	public By title=By.xpath("(//input[@class='el-input__inner'])[1]");
	public By UENNumber=By.xpath("(//input[@class='el-input__inner'])[1]");
	public By title_ComIncrInSing=By.xpath("//*[@id='pane-step3']/div/form/div[5]/div[1]/div[2]/div/div/div[1]/div[1]/input");
	
	public By mr=By.xpath("/html/body/div[2]/div[1]/div[1]/ul/li[1]");
	public By nric=By.xpath("(//input[@class='el-input__inner'])[2]");
	public By firstName=By.xpath("(//input[@class='el-input__inner'])[3]");
	public By companyName=By.xpath("//*[@id='pane-step3']/div/form/div[5]/div[1]/div[1]/div/div/div[1]/input");
	public By companyName1=By.xpath("//*[@id='CompanyName']");
	public By uENNum_ComIncrpOutSing=By.xpath("(//input[@class='el-input__inner'])[3]");
	public By firstName_ComIncrInSing=By.xpath("(//input[@class='el-input__inner'])[4]");
	public By address_ComIncrInSing=By.xpath("(//input[@class='el-input__inner'])[5]");
	public By middleName_ComIncrInSing=By.xpath("(//input[@class='el-input__inner'])[6]");
	public By lastName_ComIncrInSing=By.xpath("(//input[@class='el-input__inner'])[8]");
	public By postalCode_ComIncrInSing=By.xpath("//*[@id='pane-step3']/div/form/div[5]/div[1]/div[7]/div/div/div[1]/input");
	public By postalCode_ComIncrInSing1=By.xpath("//*[@id='pane-step3']/div/form/div[5]/div[1]/div[9]/div/div/div/input");

	public By industryDescription=By.xpath("(//input[@class='el-input__inner'])[9]");
	public By nric_ComIncrInSing=By.xpath("(//input[@class='el-input__inner'])[10]");
	public By ssic_ComIncrInSing=By.xpath("(//input[@class='el-input__inner'])[11]");
	public By companyPostalCode_ComIncrOutSing=By.xpath("(//input[@class='el-input__inner'])[7]");
	public By nationality_ComIncrInSing=By.xpath("(//input[@class='el-input__inner'])[12]");
	public By indonesia_ComIncrInSing=By.xpath("//ul[@class='el-scrollbar__view el-select-dropdown__list']//li//span[text()='Indonesia']");
	public By date_ComIncrInSing=By.xpath("(//input[@class='el-input__inner'])[13]");
	public By date_ComIncrOutSing=By.xpath("(//input[@class='el-input__inner'])[11]");
	public By day_ComIncrInSing=By.xpath("//table[@class='el-date-table']//tr//td[@class='available today current']");
	public By primaryCntNum_ComIncrInSing=By.xpath("(//input[@class='el-input__inner'])[15]");
	public By primaryCntNum_ComIncrOutSing=By.xpath("(//input[@class='el-input__inner'])[13]");
	public By designation_ComIncrOutSing=By.xpath("(//input[@class='el-input__inner'])[15]");
	public By designation_ComIncrInSing=By.xpath("(//input[@class='el-input__inner'])[16]");
	
	public By contactNo=By.xpath("(//input[@class='el-input__inner'])[4]");
	public By nationality=By.xpath("(//input[@class='el-input__inner'])[4]");
	public By nationality_Indonesia=By.xpath("//html/body/div[3]/div[1]/div[1]/ul/li[102]/span");
	public By middleName=By.xpath("(//input[@class='el-input__inner'])[5]");
	public By address=By.xpath("(//input[@class='el-input__inner'])[6]");
	public By address_NonSingapore=By.xpath("(//input[@class='el-input__inner'])[8]");
	
	public By lastName=By.xpath("(//input[@class='el-input__inner'])[7]");
	
	
	public By postalCode=By.xpath("(//input[@class='el-input__inner'])[8]");
	public By postalCode_NonSingapore=By.xpath("(//input[@class='el-input__inner'])[9]");
	
	public By registerChkbx1=By.xpath("(//span[@class='el-checkbox__inner'])[1]");
	public By registerChkbx2=By.xpath("(//span[@class='el-checkbox__inner'])[2]");
	public By registerChkbx3=By.xpath("(//span[@class='el-checkbox__inner'])[3]");
	public By registerChkbx4=By.xpath("(//span[@class='el-checkbox__inner'])[4]");
	public By submit=By.xpath("//button[@class='el-button cta el-button--primary']");
	
	public By logout=By.xpath("//span[text()='Logout']");
	
	public By continueDoc=By.xpath("(//button[@class='el-button el-button--primary'])[3]");
	
	public By upload1=By.xpath("(//div[@class='inputCustomeFile'])[1]");
	public By upload2=By.xpath("(//div[@class='inputCustomeFile'])[2]");
	public By upload3=By.xpath("(//div[@class='inputCustomeFile'])[3]");
	public By upload4=By.xpath("(//div[@class='inputCustomeFile'])[4]");
	public By upload5=By.xpath("(//div[@class='inputCustomeFile'])[5]");
	public By upload6=By.xpath("(//div[@class='inputCustomeFile'])[6]");
	
	public By liveFacilities=By.xpath("//li[@class='el-menu-item']//span[text()='Live Facilities ']");
	
	public By account=By.xpath("(//div[@class='el-submenu__title'])[3]");
	public By bank=By.xpath("//li[@class='el-menu-item']/span[text()='Bank']");
	public By nameasperBankAccount=By.xpath("//input[@placeholder=\"Name as per Bank Account\"]");
	public By bankName=By.xpath("//input[@placeholder=\"Bank name\"]");
	public By accountNumber=By.xpath("//input[@placeholder=\"Account number\"]");
	public By branchCode=By.xpath("//input[@placeholder=\"Branch code\"]");
	public By submit_Bank=By.xpath("//button[@type=\"button\"]/span[text()='SUBMIT']");
    
	public By fund=By.xpath("(//button[@class='el-button btn-decor el-button--primary el-button--small is-plain']//span)[1]");
	public By fundAmount=By.xpath("(//div[@class='el-dialog__body']//p/span)[2]");
	public By investmentCommitment=By.xpath("//input[@class='inputAmountInvest' and @placeholder='SGD']");
	public By fundButton=By.xpath("//button[@class='el-button el-button--primary']/span[text()='FUND']");
	public By fundCHKbox1=By.xpath("(//span[@class='el-checkbox__inner'])[1]");
	public By fundCHKbox2=By.xpath("(//span[@class='el-checkbox__inner'])[2]");
	public By fundCHKbox3=By.xpath("(//span[@class='el-checkbox__inner'])[3]");
	public By fundCHKbox4=By.xpath("(//span[@class='el-checkbox__inner'])[4]");
	public By confirm=By.xpath("//span[contains(text(),'CONFIRM')]");
	public By fundCancel=By.xpath("(//button[@class='el-button el-button--default']/span[text()='CANCEL'])[1]");
	
	
	public By withdraw=By.xpath("//li[@class='el-menu-item']/span[text()='Withdraw']");
	public By balanceAMT=By.xpath("//span[@class=\"balanceAmount\"]");
	public By enterAMT=By.xpath("//input[@placeholder=\"enter the amount\"]");
	public By withDraw=By.xpath("//button[@class='el-button el-button--primary']/span[text()='WITHDRAW']");
	public By confirmMSG=By.xpath("//button[@class='el-button el-button--default el-button--small el-button--primary ']");	
	
	public By historicalReports=By.xpath("//span[text()='Historical Reports']");	
	public By transactions=By.xpath("//span[text()='Transactions']");
}


















