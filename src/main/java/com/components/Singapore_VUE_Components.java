package com.components;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.sikuli.script.Screen;

import com.baseClasses.BaseClass_Web;
import com.baseClasses.PDFResultReport;
import com.baseClasses.ThreadLocalWebdriver;
import com.objectRepository.Singapore_VUE_Locators;
import com.objectRepository.ValidusRegisterAsInvester;
import com.objectRepository.ValidusSmoke_Loc;


public class Singapore_VUE_Components extends BaseClass_Web {

//	public ValidusSmoke_Loc validussmokelocators = new ValidusSmoke_Loc();
	public Screen s = new Screen();
	public Singapore_VUE_Locators singapore_VUE_Locators=new Singapore_VUE_Locators();
	public ValidusRegisterAsInvester registerasInvestorlocators=new ValidusRegisterAsInvester();
	public static String emailIDInvestor;
	public static String emailIDInvestor1;
	public Singapore_VUE_Components(PDFResultReport pdfresultReport) {
		this.pdfResultReport = pdfresultReport;
	}

	public void openURL() throws Exception {
		try {
			launchapp(pdfResultReport.testDataValue.get("AppURL"));
			pdfResultReport.addStepDetails("openURL", "Application should open the url",
					"Successfully opened the URL" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to open the URL" + e.getMessage());
			pdfResultReport.addStepDetails("openURL", "Application should open the url",
					"Unable to open the URL" + e.getMessage(), "Fail", "N");
		}

	}

	public void CreateAccount() throws Throwable {
		try {
			click(singapore_VUE_Locators.registerNow);
			waitForElement(singapore_VUE_Locators.email, 30);
			waitForObj(3000);
			SimpleDateFormat df = new SimpleDateFormat("MMddhhmmss");
			Date d = new Date();
			String time = df.format(d);
			System.out.println("time::" + time);
			emailIDInvestor1="nag"+time;
			System.out.println("emailIDInvestor1 ::"+emailIDInvestor1);
			emailIDInvestor = emailIDInvestor1 + "@gmailx.com";
			System.out.println("emailIDInvestor :::"+emailIDInvestor);
			set(singapore_VUE_Locators.email, emailIDInvestor);
			set(singapore_VUE_Locators.password, pdfResultReport.testData.get("PasswordInvestor"));
			set(singapore_VUE_Locators.confirmPassword, pdfResultReport.testData.get("ConfirmPasswordInvestor"));
			SimpleDateFormat df1 = new SimpleDateFormat("hhmmss");
			Date d1 = new Date();
			String time1 = df1.format(d1);
			String time2 = "621"+time1;
			System.out.println("time2::" + time2);
			Thread.sleep(2000);
			set(singapore_VUE_Locators.mobileNo, time2);
			click(singapore_VUE_Locators.checkBX);
			Thread.sleep(2000);
			click(singapore_VUE_Locators.continueBtn);
			Thread.sleep(4000);
			pdfResultReport.addStepDetails("Create account", "Application should allow the user to enter details",
					"Successfully created an account" + " ", "Pass", "Y");

		} catch (Exception e) {
			System.out.println(e);
			log.fatal("Unable to create account" + e.getMessage() + "Line Number :" + e.getStackTrace());
			pdfResultReport.addStepDetails("Create account", "Unable to enter details",
					"Unable to create account" + e.getMessage(), "Fail", "N");

		}
	}
	
	public void CreateAccount_InvestorNEW() throws Throwable {
		try {
		//	click(singapore_VUE_Locators.registerNow);
		//	waitForElement(singapore_VUE_Locators.email, 30);
		//	waitForObj(3000);
			click(singapore_VUE_Locators.register);
			click(singapore_VUE_Locators.investor);
			Thread.sleep(2000);
			SimpleDateFormat df = new SimpleDateFormat("hhmmss");
			Date d = new Date();
			String time = df.format(d);
			System.out.println("time::" + time);
			String text="naga";
			emailIDInvestor1=text+time;
			System.out.println("emailIDInvestor1 ::"+emailIDInvestor1);
			emailIDInvestor = emailIDInvestor1 + "@gmailx.com";
			System.out.println("emailIDInvestor :::"+emailIDInvestor);
			Thread.sleep(1000);
			click(singapore_VUE_Locators.emailNew);
		//	set(By.xpath("//input[@class='email invalid']"), emailIDInvestor);
			Screen s=new Screen();
			Robot r=new Robot();
			r.keyPress(KeyEvent.VK_CONTROL);
			r.keyRelease(KeyEvent.VK_CONTROL);
			Thread.sleep(2000);
			s.type(System.getProperty("user.dir").replace("\\", "/") + "/MediaFiles/username.png", emailIDInvestor);
			Thread.sleep(2000);
			r.keyPress(KeyEvent.VK_CONTROL);
			r.keyRelease(KeyEvent.VK_CONTROL);
			Thread.sleep(2000);
		
			SimpleDateFormat df1 = new SimpleDateFormat("hhmmss");
			Date d1 = new Date();
			String time1 = df1.format(d1);
			String time2 = "62"+time1;
			System.out.println("time2::" + time2);
			//Thread.sleep(2000);
			click(singapore_VUE_Locators.mobileNoNew);
			Thread.sleep(1000);
			//set(singapore_VUE_Locators.mobileNoNew, time2);
			Thread.sleep(1000);
			js_type(singapore_VUE_Locators.mobileNoNew, time2, "MobileNumber");
			Thread.sleep(1000);
			click(singapore_VUE_Locators.mobileNoNew);
			tab();
			Thread.sleep(1000);
			js_type(singapore_VUE_Locators.mobileNoNew, time2, "MobileNumber");
		//	Robot r = new Robot();
			r.keyPress(KeyEvent.VK_TAB);
			r.keyRelease(KeyEvent.VK_TAB);
			click(singapore_VUE_Locators.mobileNoNew);
			Thread.sleep(7000);
			ThreadLocalWebdriver.getDriver().findElement(singapore_VUE_Locators.mobileNoNew).sendKeys("99");
			Thread.sleep(4000);
			set(singapore_VUE_Locators.passwordNew, "Admin@1234");
			
			Thread.sleep(2000);
			tab();
			set(singapore_VUE_Locators.confirmPasswordNew, "Admin@1234");
			Thread.sleep(2000);
			tab();
			
			JSClick(singapore_VUE_Locators.acknowledgement, "Checkbox");
			Thread.sleep(2000);
			tab();
			Thread.sleep(2000);
			click(singapore_VUE_Locators.continueBtnNew);
			Thread.sleep(4000);
			pdfResultReport.addStepDetails("Create account", "Application should allow the user to enter details",
					"Successfully created an account" + " ", "Pass", "Y");

		} catch (Exception e) {
			System.out.println(e);
			log.fatal("Unable to create account" + e.getMessage() + "Line Number :" + e.getStackTrace());
			pdfResultReport.addStepDetails("Create account", "Unable to enter details",
					"Unable to create account" + e.getMessage(), "Fail", "N");

		}
	}
	public void CreateAccount_SMENEW() throws Throwable {
		try {
		//	click(singapore_VUE_Locators.registerNow);
		//	waitForElement(singapore_VUE_Locators.email, 30);
		//	waitForObj(3000);
			click(singapore_VUE_Locators.register);
			click(singapore_VUE_Locators.sme);
			Thread.sleep(1000);
			SimpleDateFormat df = new SimpleDateFormat("hhmmss");
			Date d = new Date();
			String time = df.format(d);
			System.out.println("time::" + time);
			String text="naga";
			emailIDInvestor1=text+time;
			System.out.println("emailIDInvestor1 ::"+emailIDInvestor1);
			emailIDInvestor = emailIDInvestor1 + "@gmailx.com";
			System.out.println("emailIDInvestor :::"+emailIDInvestor);
			Thread.sleep(1000);
			click(singapore_VUE_Locators.emailNew);
		//	set(By.xpath("//input[@class='email invalid']"), emailIDInvestor);
			Screen s=new Screen();
			Robot r=new Robot();
			r.keyPress(KeyEvent.VK_CONTROL);
			r.keyRelease(KeyEvent.VK_CONTROL);
			Thread.sleep(2000);
			s.type(System.getProperty("user.dir").replace("\\", "/") + "/MediaFiles/username.png", emailIDInvestor);
			Thread.sleep(2000);
			r.keyPress(KeyEvent.VK_CONTROL);
			r.keyRelease(KeyEvent.VK_CONTROL);
			Thread.sleep(2000);
            SimpleDateFormat df1 = new SimpleDateFormat("hhmmss");
			Date d1 = new Date();
			String time1 = df1.format(d1);
			String time2 = "62"+time1;
			System.out.println("time2::" + time2);
			//Thread.sleep(2000);
			click(singapore_VUE_Locators.mobileNoNew);
			Thread.sleep(1000);
			//set(singapore_VUE_Locators.mobileNoNew, time2);
			Thread.sleep(1000);
			js_type(singapore_VUE_Locators.mobileNoNew, time2, "MobileNumber");
			Thread.sleep(1000);
			click(singapore_VUE_Locators.mobileNoNew);
			tab();
			Thread.sleep(1000);
			js_type(singapore_VUE_Locators.mobileNoNew, time2, "MobileNumber");
		//	Robot r = new Robot();
			r.keyPress(KeyEvent.VK_TAB);
			r.keyRelease(KeyEvent.VK_TAB);
			click(singapore_VUE_Locators.mobileNoNew);
			Thread.sleep(2000);
			ThreadLocalWebdriver.getDriver().findElement(singapore_VUE_Locators.mobileNoNew).sendKeys("99");
			Thread.sleep(1000);
			set(singapore_VUE_Locators.passwordNew, "Admin@1234");
			
			Thread.sleep(1000);
			tab();
			set(singapore_VUE_Locators.confirmPasswordNew, "Admin@1234");
			Thread.sleep(1000);
			tab();
			
			JSClick(singapore_VUE_Locators.acknowledgement, "Checkbox");
			Thread.sleep(1000);
			tab();
			Thread.sleep(1000);
			click(singapore_VUE_Locators.continueBtnNew);
			Thread.sleep(1000);
			pdfResultReport.addStepDetails("Create account", "Application should allow the user to enter details",
					"Successfully created an account" + " ", "Pass", "Y");

		} catch (Exception e) {
			System.out.println(e);
			log.fatal("Unable to create account" + e.getMessage() + "Line Number :" + e.getStackTrace());
			pdfResultReport.addStepDetails("Create account", "Unable to enter details",
					"Unable to create account" + e.getMessage(), "Fail", "N");

		}
	}
	
	
	public void login() throws Throwable {
		try {
			
			//set(singapore_VUE_Locators.loginMail, "naga111@gmailx.com");
			Thread.sleep(2000);
		//	set(singapore_VUE_Locators.loginMail, emailIDInvestor);
			Screen s=new Screen();
			
			s.type(System.getProperty("user.dir").replace("\\", "/") + "/MediaFiles/usernameLinux.png", emailIDInvestor);
		
			//s.type(System.getProperty("user.dir").replace("\\", "/") + "/MediaFiles/usernameLinux.png", "naga095844@gmailx.com");
			set(singapore_VUE_Locators.loginPassword, "Admin@1234");
		//	click(singapore_VUE_Locators.loginCheckBX);
			Thread.sleep(2000);
			click(singapore_VUE_Locators.login);
			System.out.println("Clicked on login");

		//	
			Thread.sleep(3000);
			pdfResultReport.addStepDetails("login", "Application should allow the user to login",
					"Successfully loggedin" + " ", "Pass", "Y");

		} catch (Exception e) {
			System.out.println(e);
			log.fatal("Unable to create account" + e.getMessage() + "Line Number :" + e.getStackTrace());
			pdfResultReport.addStepDetails("login", "Login into the application",
					"Unable to login" + e.getMessage(), "Fail", "N");

		}
	}
	
	public void navigateToIndividualInvestorSingaporeCitizen() throws Throwable {
		try {
			click(singapore_VUE_Locators.individualInvestorSingaporeCitizen);
			pdfResultReport.addStepDetails("individualInvestorSingaporeCitizen", "Application should click on individual Investor Singapore Citizen",
					"Successfully clicked on individual Investor Singapore Citizen" + " ", "Pass", "Y");

		} catch (Exception e) {
			System.out.println(e);
			log.fatal("Unable to create account" + e.getMessage() + "Line Number :" + e.getStackTrace());
			pdfResultReport.addStepDetails("login", "Application should click on individual Investor Singapore Citizen",
					"Unable to click on individual Investor Singapore Citizen" + e.getMessage(), "Fail", "N");

		}
	}
	public void navigateToIndividualInvestorNonSingaporeCitizen() throws Throwable {
		try {
			click(singapore_VUE_Locators.individualInvestorNONSingaporeCitizen);
			pdfResultReport.addStepDetails("navigateToIndividualInvestorNonSingaporeCitizen", "Application should click on Individual Investor Non SingaporeCitizen",
					"Successfully clicked on Individual Investor Non SingaporeCitizen" + " ", "Pass", "Y");

		} catch (Exception e) {
			System.out.println(e);
			log.fatal("Unable to create account" + e.getMessage() + "Line Number :" + e.getStackTrace());
			pdfResultReport.addStepDetails("navigateToIndividualInvestorNonSingaporeCitizen", "Application should click on Individual Investor Non SingaporeCitizen",
					"Unable to click on Individual Investor Non SingaporeCitizen" + e.getMessage(), "Fail", "N");

		}
	}
	
	public void navigateTocompanyIncorporatedInSingapore() throws Throwable {
		try {
			waitForElement(singapore_VUE_Locators.companyIncorporatedInSingapore, 30);
			waitForObj(3000);
			click(singapore_VUE_Locators.companyIncorporatedInSingapore);
			pdfResultReport.addStepDetails("navigateTocompanyIncorporatedInSingapore", "Application should click on companyIncorporatedInSingapore",
					"Successfully clicked on companyIncorporatedInSingapore" + " ", "Pass", "Y");

		} catch (Exception e) {
			System.out.println(e);
			log.fatal("Unable to create account" + e.getMessage() + "Line Number :" + e.getStackTrace());
			pdfResultReport.addStepDetails("navigateTocompanyIncorporatedInSingapore", "Application should click on companyIncorporatedInSingapore",
					"Unable to click on companyIncorporatedInSingapore" + e.getMessage(), "Fail", "N");

		}
	}
	
	public void navigateTocompanyIncorporatedOutsideSingapore() throws Throwable {
		try {
			waitForElement(singapore_VUE_Locators.individualInvestorSingaporeCitizen, 30);
			click(singapore_VUE_Locators.companyIncorporatedOutsideSingapore);
			pdfResultReport.addStepDetails("navigateTocompanyIncorporatedInSingapore", "Application should click on companyIncorporatedOutsideSingapore",
					"Successfully clicked on companyIncorporatedInSingapore" + " ", "Pass", "Y");

		} catch (Exception e) {
			System.out.println(e);
			log.fatal("Unable to create account" + e.getMessage() + "Line Number :" + e.getStackTrace());
			pdfResultReport.addStepDetails("navigateTocompanyIncorporatedInSingapore", "Application should click on companyIncorporatedOutsideSingapore",
					"Unable to click on companyIncorporatedOutsideSingapore" + e.getMessage(), "Fail", "N");

		}
	}
	
	public void uploadFile(By loc) throws Throwable {
		try {
			//
		//	Thread.sleep(2000);
			click(loc);
			Thread.sleep(4000);
			Robot r=new Robot();
			r.keyPress(KeyEvent.VK_CONTROL);
			r.keyRelease(KeyEvent.VK_CONTROL);
			Thread.sleep(1000);
		//	s.click(System.getProperty("user.dir").replace("\\", "/") + "/MediaFiles/testtpdf.png");
		//	s.click(System.getProperty("user.dir").replace("\\", "/") + "/MediaFiles/openn.png");
		//	s.type(System.getProperty("user.dir").replace("\\", "/") + "/MediaFiles/File.PNG", System.getProperty("user.dir")+"\\MediaFiles\\"+fileName+".pdf");
			s.click(System.getProperty("user.dir").replace("\\", "/") + "/MediaFiles/pdffileLinux.png");
			//s.click(System.getProperty("user.dir").replace("\\", "/") + "/MediaFiles/openLinux.png");
			Thread.sleep(1000);
			  r.keyPress(KeyEvent.VK_ENTER);
			  r.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(4000);
		} catch (Exception e) {
			System.out.println("Unable to upload the file");
		}
	}
	public void uploadFile(By loc, String fileName) throws Throwable {
		try {
			//
		//	Thread.sleep(2000);
			click(loc);
			Thread.sleep(4000);
			Robot r=new Robot();
			r.keyPress(KeyEvent.VK_CONTROL);
			r.keyRelease(KeyEvent.VK_CONTROL);
			Thread.sleep(1000);
		//	s.click(System.getProperty("user.dir").replace("\\", "/") + "/MediaFiles/testtpdf.png");
		//	s.click(System.getProperty("user.dir").replace("\\", "/") + "/MediaFiles/openn.png");
			s.type(System.getProperty("user.dir").replace("\\", "/") + "/MediaFiles/File.PNG", System.getProperty("user.dir")+"\\MediaFiles\\"+fileName+".pdf");
			//s.click(System.getProperty("user.dir").replace("\\", "/") + "/MediaFiles/OpenWin.PNG");
			Thread.sleep(1000);
			  r.keyPress(KeyEvent.VK_ENTER);
			  r.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(4000);
		} catch (Exception e) {
			System.out.println("Unable to upload the file");
		}
	}
	
	public void fillIndividualInvestorSingaporeCitizen() throws Throwable {
		try {
			click(singapore_VUE_Locators.title);
			Thread.sleep(1000);
			click(singapore_VUE_Locators.mr);
			set(singapore_VUE_Locators.nric, pdfResultReport.testData.get("NRIC/Passport Number"));
			set(singapore_VUE_Locators.firstName, emailIDInvestor1);
			set(singapore_VUE_Locators.middleName, pdfResultReport.testData.get("Middle Name"));
			set(singapore_VUE_Locators.address, pdfResultReport.testData.get("Registered Address"));
			set(singapore_VUE_Locators.lastName, pdfResultReport.testData.get("Last Name"));
			set(singapore_VUE_Locators.postalCode, pdfResultReport.testData.get("Postal code"));
			pdfResultReport.addStepDetails("fillIndividualInvestorSingaporeCitizen",
					"User should register as IndividualInvestorSingaporeCitizen",
					"Successfully registered as IndividualInvestorSingaporeCitizen" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("fillIndividualInvestorSingaporeCitizen",
					"User should register as IndividualInvestorSingaporeCitizen",
					"Unable to register as IndividualInvestorSingaporeCitizen" + e.getMessage(), "Fail", "N");
		}
	}
	
	public void fillindividualInvestorNONSingaporeCitizen() throws Throwable {
		try {
			click(singapore_VUE_Locators.title);
			Thread.sleep(1000);
			click(singapore_VUE_Locators.mr);
			set(singapore_VUE_Locators.nric, pdfResultReport.testData.get("NRIC/Passport Number"));
		//	set(singapore_VUE_Locators.firstName, pdfResultReport.testData.get("First Name"));
			set(singapore_VUE_Locators.firstName, "EswarRao");
			set(singapore_VUE_Locators.nationality, "Indonesia");
			click(singapore_VUE_Locators.nationality_Indonesia);
			set(singapore_VUE_Locators.middleName, pdfResultReport.testData.get("Middle Name"));
			set(singapore_VUE_Locators.lastName, pdfResultReport.testData.get("Last Name"));
			set(singapore_VUE_Locators.address_NonSingapore, pdfResultReport.testData.get("Registered Address"));
			
			set(singapore_VUE_Locators.postalCode_NonSingapore, pdfResultReport.testData.get("Postal code"));
			pdfResultReport.addStepDetails("fillIndividualInvestorSingaporeCitizen",
					"User should register as IndividualInvestorSingaporeCitizen",
					"Successfully registered as IndividualInvestorSingaporeCitizen" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("fillIndividualInvestorSingaporeCitizen",
					"User should register as IndividualInvestorSingaporeCitizen",
					"Unable to register as IndividualInvestorSingaporeCitizen" + e.getMessage(), "Fail", "N");
		}
	}
	
	public void fillcompanyIncorporatedInSingapore() throws Throwable {
		try {
			set(singapore_VUE_Locators.UENNumber, pdfResultReport.testData.get("NRIC/Passport Number"));
			
			click(singapore_VUE_Locators.title_ComIncrInSing);
			Thread.sleep(1000);
			click(singapore_VUE_Locators.mr);
		//	set(singapore_VUE_Locators.companyName, pdfResultReport.testData.get("Companyname"));
			set(singapore_VUE_Locators.companyName, emailIDInvestor1);
			tab();
			set(singapore_VUE_Locators.firstName_ComIncrInSing, pdfResultReport.testData.get("First Name"));
			set(singapore_VUE_Locators.address_ComIncrInSing, pdfResultReport.testData.get("Registered Address"));
			set(singapore_VUE_Locators.middleName_ComIncrInSing, pdfResultReport.testData.get("Middle Name"));
			set(singapore_VUE_Locators.companyName1, pdfResultReport.testData.get("Companyname"));
			set(singapore_VUE_Locators.lastName_ComIncrInSing, pdfResultReport.testData.get("Last Name"));
			set(singapore_VUE_Locators.postalCode_ComIncrInSing1, pdfResultReport.testData.get("Postal code"));
			tab();
			set(singapore_VUE_Locators.nric_ComIncrInSing, pdfResultReport.testData.get("NRIC/Passport Number"));
			set(singapore_VUE_Locators.ssic_ComIncrInSing, pdfResultReport.testData.get("SICC (Industry) Code"));
			set(singapore_VUE_Locators.nationality_ComIncrInSing, "Indonesia");
			Thread.sleep(1000);
			click(singapore_VUE_Locators.indonesia_ComIncrInSing);
			
			click(singapore_VUE_Locators.date_ComIncrInSing);
			Thread.sleep(1000);
			click(singapore_VUE_Locators.day_ComIncrInSing);
			
			set(singapore_VUE_Locators.primaryCntNum_ComIncrInSing, pdfResultReport.testData.get("Primary Contact Number"));
			set(singapore_VUE_Locators.designation_ComIncrInSing, pdfResultReport.testData.get("Designation"));
			
			pdfResultReport.addStepDetails("fillIndividualInvestorSingaporeCitizen",
					"User should register as IndividualInvestorSingaporeCitizen",
					"Successfully registered as IndividualInvestorSingaporeCitizen" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("fillIndividualInvestorSingaporeCitizen",
					"User should register as IndividualInvestorSingaporeCitizen",
					"Unable to register as IndividualInvestorSingaporeCitizen" + e.getMessage(), "Fail", "N");
		}
	}
	
	public void fillcompanyIncorporatedOutSingapore() throws Throwable {
		try {
			//set(singapore_VUE_Locators.firstName,  emailIDInvestor1);
			set(singapore_VUE_Locators.companyName, pdfResultReport.testData.get("Companyname"));
			click(singapore_VUE_Locators.title_ComIncrInSing);
			

			Thread.sleep(1000);
			click(singapore_VUE_Locators.mr);
			
			set(singapore_VUE_Locators.uENNum_ComIncrpOutSing, pdfResultReport.testData.get("UEN Number"));
			tab();
			set(singapore_VUE_Locators.firstName_ComIncrInSing, "Indonesia");
			set(singapore_VUE_Locators.address_ComIncrInSing, pdfResultReport.testData.get("Registered Address"));
			set(singapore_VUE_Locators.middleName_ComIncrInSing, pdfResultReport.testData.get("Middle Name"));
			set(singapore_VUE_Locators.lastName_ComIncrInSing, pdfResultReport.testData.get("Last Name"));
			set(singapore_VUE_Locators.postalCode_ComIncrInSing, pdfResultReport.testData.get("Postal code"));
			set(singapore_VUE_Locators.industryDescription, pdfResultReport.testData.get("Last Name"));
            tab();
			set(singapore_VUE_Locators.nric_ComIncrInSing, pdfResultReport.testData.get("NRIC/Passport Number"));
			//set(singapore_VUE_Locators.companyPostalCode_ComIncrOutSing, pdfResultReport.testData.get("Postal code"));
			click(singapore_VUE_Locators.nationality_ComIncrInSing);
			click(By.xpath("/html/body/div[4]/div[1]/div[1]/ul/li[5]/span"));
			//selectbyIndex(singapore_VUE_Locators.nationality_ComIncrInSing, 2);
			Thread.sleep(1000);
			set(singapore_VUE_Locators.primaryCntNum_ComIncrOutSing, pdfResultReport.testData.get("PrimaryCntctNo"));
			//click(singapore_VUE_Locators.indonesia_ComIncrInSing);
			
			click(singapore_VUE_Locators.date_ComIncrOutSing);
			Thread.sleep(1000);
			click(singapore_VUE_Locators.day_ComIncrInSing);
			
			set(singapore_VUE_Locators.designation_ComIncrOutSing, pdfResultReport.testData.get("Designation"));
			
			
			pdfResultReport.addStepDetails("fillIndividualInvestorSingaporeCitizen",
					"User should register as IndividualInvestorSingaporeCitizen",
					"Successfully registered as IndividualInvestorSingaporeCitizen" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("fillIndividualInvestorSingaporeCitizen",
					"User should register as IndividualInvestorSingaporeCitizen",
					"Unable to register as IndividualInvestorSingaporeCitizen" + e.getMessage(), "Fail", "N");
		}
	}
	public void uploadFiles_IndividualInvestorSingaporeCitizen() throws Throwable {
		try {	
			uploadFile(singapore_VUE_Locators.upload1);
			waitForObj(7000);
			uploadFile(singapore_VUE_Locators.upload2);
			waitForObj(7000);
			uploadFile(singapore_VUE_Locators.upload3);
			waitForObj(7000);
			uploadFile(singapore_VUE_Locators.upload4);
			waitForObj(7000);
			
			click(singapore_VUE_Locators.registerChkbx1);
			click(singapore_VUE_Locators.registerChkbx2);
			click(singapore_VUE_Locators.registerChkbx3);
			click(singapore_VUE_Locators.registerChkbx4);
			click(singapore_VUE_Locators.submit);
			Thread.sleep(6000);
			
			pdfResultReport.addStepDetails("fillIndividualInvestorSingaporeCitizen",
					"User should register as IndividualInvestorSingaporeCitizen",
					"Successfully registered as IndividualInvestorSingaporeCitizen" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("fillIndividualInvestorSingaporeCitizen",
					"User should register as IndividualInvestorSingaporeCitizen",
					"Unable to register as IndividualInvestorSingaporeCitizen" + e.getMessage(), "Fail", "N");
		}
	}
	public void uploadFiles_companyIncorporatedInSingaporeCitizen() throws Throwable {
		try {	
			uploadFile(singapore_VUE_Locators.upload1);
			waitForObj(7000);
			uploadFile(singapore_VUE_Locators.upload2);
			waitForObj(7000);
			uploadFile(singapore_VUE_Locators.upload3);
			waitForObj(7000);
			uploadFile(singapore_VUE_Locators.upload4);
			waitForObj(7000);
			uploadFile(singapore_VUE_Locators.upload5);
			waitForObj(7000);
			uploadFile(singapore_VUE_Locators.upload6);
			waitForObj(7000);
			click(singapore_VUE_Locators.registerChkbx1);
			click(singapore_VUE_Locators.registerChkbx2);
			click(singapore_VUE_Locators.registerChkbx3);
			click(singapore_VUE_Locators.registerChkbx4);
			click(singapore_VUE_Locators.submit);
			waitForObj(3000);
			pdfResultReport.addStepDetails("fillIndividualInvestorSingaporeCitizen",
					"User should register as IndividualInvestorSingaporeCitizen",
					"Successfully registered as IndividualInvestorSingaporeCitizen" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("fillIndividualInvestorSingaporeCitizen",
					"User should register as IndividualInvestorSingaporeCitizen",
					"Unable to register as IndividualInvestorSingaporeCitizen" + e.getMessage(), "Fail", "N");
		}
	}
	public void submit_Continue() throws Throwable {
		try {	
			waitForElement(singapore_VUE_Locators.submit, 30);
			click(singapore_VUE_Locators.submit);
			waitForElement(singapore_VUE_Locators.continueDoc, 30);
			waitForObj(10000);
			click(singapore_VUE_Locators.continueDoc);
			pdfResultReport.addStepDetails("submit_Continue",
					"User should click on submit & Continue",
					"Successfully clicked on submit & Continue" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("submit " + e.getMessage());
			pdfResultReport.addStepDetails("submit_Continue",
					"User should click on submit & Continue",
					"Unable to click on submit & Continue" + e.getMessage(), "Fail", "N");
		}
	}
	
	public void clickContinue() throws Throwable {
		try {	

			//click(singapore_VUE_Locators.submit);
			waitForElement(singapore_VUE_Locators.continueDoc, 30);
			waitForObj(10000);
			click(singapore_VUE_Locators.continueDoc);
			pdfResultReport.addStepDetails("clickContinue",
					"User should click on Continue",
					"Successfully clicked on Continue" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("submit " + e.getMessage());
			pdfResultReport.addStepDetails("clickContinue",
					"User should click on Continue",
					"Unable to click on Continue" + e.getMessage(), "Fail", "N");
		}
	}
	public void switchToApp() throws Throwable {
		try {	

			switchwindow(1);
		} catch (Exception e) {
			log.fatal("submit " + e.getMessage());
			pdfResultReport.addStepDetails("clickContinue",
					"User should click on Continue",
					"Unable to click on Continue" + e.getMessage(), "Fail", "N");
		}
	}
	public void logout() throws Throwable {
		try {	

			click(singapore_VUE_Locators.logout);
		
			pdfResultReport.addStepDetails("logout",
					"User should logout from the application",
					"Successfully logged out from the application" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("logout " + e.getMessage());
			pdfResultReport.addStepDetails("logout",
					"User should logout from the application",
					"Unable to logout from the application" + e.getMessage(), "Fail", "N");
		}
	}
	public void allliveFacilities() throws Throwable {
		try {	
			waitForElement(singapore_VUE_Locators.liveFacilities, 3);
			click(singapore_VUE_Locators.liveFacilities);
			Thread.sleep(6000);
			pdfResultReport.addStepDetails("allliveFacilities",
					"User should click on liveFacilities",
					"Successfully clicked on liveFacilities" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("allliveFacilities " + e.getMessage());
			pdfResultReport.addStepDetails("allliveFacilities",
					"User should click on liveFacilities",
					"Unable to click on liveFacilities" + e.getMessage(), "Fail", "N");
		}
	}
	
	public void fund() throws Throwable {
		try {	
		//	waitForElement(singapore_VUE_Locators.fund, 1);
			
			click(singapore_VUE_Locators.fund);
			Thread.sleep(3000);
			String fundAmt=text(singapore_VUE_Locators.fundAmount);
			String[] amt= fundAmt.split(":");
			System.out.println(amt[1]);
			String fundAmount=amt[1].trim();
            click(By.xpath("//input[@class='inputAmountInvest' and @placeholder='SGD']"));
		//	set(singapore_VUE_Locators.investmentCommitment, fundAmount);
        	set(singapore_VUE_Locators.investmentCommitment, "1");
			tab();
			click(singapore_VUE_Locators.fundButton);
			Thread.sleep(2000);
			click(singapore_VUE_Locators.fundCHKbox1);
			click(singapore_VUE_Locators.fundCHKbox2);
			click(singapore_VUE_Locators.fundCHKbox3);
			click(singapore_VUE_Locators.fundCHKbox4);
			Thread.sleep(2000);
			click(singapore_VUE_Locators.confirm);
			Thread.sleep(5000);
			pdfResultReport.addStepDetails("fund",
					"User should fund the amount",
					"Successfully  funded the amount" + " ", "Pass", "Y");
		} catch (Exception e) {
			click(singapore_VUE_Locators.fundCancel);
			Thread.sleep(2000);
			log.fatal("Unable to  fund the amount " + e.getMessage());
			System.out.println("Records are not available to Fund");
			pdfResultReport.addStepDetails("fund",
					"User should fund the amount",
					"Records are not available to Fund : " + e.getMessage(), "Fail", "N");
		}
		
/*		try {
			click(singapore_VUE_Locators.fundCancel);
			Thread.sleep(2000);
			click(singapore_VUE_Locators.fundCancel);
			System.out.println("Issue in funding...Clicked on Cancel");
		} catch (Exception e) {
			System.out.println("Catch in fund cancel ");
		}*/
	}
	
	
	public void bankDetails() throws Throwable {
		try {
			click(singapore_VUE_Locators.account);
			click(singapore_VUE_Locators.bank);
			Thread.sleep(2000);
			waitForElement(singapore_VUE_Locators.nameasperBankAccount, 10);
			set(singapore_VUE_Locators.nameasperBankAccount, pdfResultReport.testData.get("Name As per Bank"));
			set(singapore_VUE_Locators.bankName, pdfResultReport.testData.get("Bank Name"));
			set(singapore_VUE_Locators.accountNumber, pdfResultReport.testData.get("Account Number"));
			set(singapore_VUE_Locators.branchCode, pdfResultReport.testData.get("Branch Code"));
			click(singapore_VUE_Locators.submit_Bank);
			Thread.sleep(2000);
			pdfResultReport.addStepDetails("bankDetails", "Application should enter the bank details",
					"Successfully entered the bank details" + " ", "Pass", "Y");

		} catch (Exception e) {
			System.out.println(e);
			log.fatal("Unable to create account" + e.getMessage() + "Line Number :" + e.getStackTrace());
			pdfResultReport.addStepDetails("bankDetails", "Application should enter the bank details",
					"Unable to enter the bank details" + e.getMessage(), "Fail", "N");

		}
	}
	
	public void withDraw() throws Throwable {
		try {
			Thread.sleep(2000);
		//	click(singapore_VUE_Locators.account);
			click(singapore_VUE_Locators.withdraw);
			Thread.sleep(4000);
			waitForElement(singapore_VUE_Locators.balanceAMT, 10);
			String balanceAMT=text(singapore_VUE_Locators.balanceAMT);
			System.out.println("balanceAMT :::::" +balanceAMT);
			
			set(singapore_VUE_Locators.enterAMT, "2000");
			click(singapore_VUE_Locators.withDraw);
			Thread.sleep(3000);
			click(singapore_VUE_Locators.confirmMSG);
			Thread.sleep(4000);
			pdfResultReport.addStepDetails("withDraw", "Application should withDraw the money",
					"Successfully withDrawed the money" + " ", "Pass", "Y");

		} catch (Exception e) {
			System.out.println(e);
			log.fatal("Unable to create account" + e.getMessage() + "Line Number :" + e.getStackTrace());
			pdfResultReport.addStepDetails("withDraw", "Application should withDraw the money",
					"Unable to withDraw the money" + e.getMessage(), "Fail", "N");

		}
	}
	
	public void reports() throws Throwable {
		try {
			switchwindow(1);
			Thread.sleep(1000);
			click(singapore_VUE_Locators.historicalReports);
			click(singapore_VUE_Locators.transactions);
			Thread.sleep(4000);
			String table = ThreadLocalWebdriver.getDriver().findElement(By.xpath("//table[@class='el-table__body']")).getText();
			System.out.println("table   :::::"+table);
/*			System.out.println("-----------------");
			List<WebElement> iRows = ThreadLocalWebdriver.getDriver().findElements(By.xpath("//table[@class=\"el-table__body\"]//tr"));
			System.out.println(iRows.size());
			for (int i = 0; i < iRows.size(); i++) {
				System.out.println(iRows.get(i).getText());
			}*/
			
			
			pdfResultReport.addStepDetails("reports", "Application should display the transactions" +table,
					"Successfully withDrawed the money" + " ", "Pass", "Y");

		} catch (Exception e) {
			System.out.println(e);
			log.fatal("Unable to display the transactions" + e.getMessage() + "Line Number :" + e.getStackTrace());
			pdfResultReport.addStepDetails("reports", "Application should display the transactions",
					"Unable to display the transactions" + e.getMessage(), "Fail", "N");

		}
	}
	

}
