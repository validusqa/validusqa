package com.testScripts_Singapore;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.baseClasses.BaseClass_Web;
import com.components.Singapore_VUE_Components;
import com.components.Singapore_VUE_SalesforceComponents;
import com.components.ValidusRegisterAsInvestorComponents;
import com.components.ValidusSalesforceComponents;
import com.components.ValidusSmoke_Components;

public class Singapore_CompanyIncorporatedInSingapore extends BaseClass_Web{
	public ValidusRegisterAsInvestorComponents investorComponents =new ValidusRegisterAsInvestorComponents(pdfResultReport); 
//	public ValidusSmoke_Components smokeComponent =new ValidusSmoke_Components(pdfResultReport); 
	public Singapore_VUE_Components singapore_VUE_Components =new Singapore_VUE_Components(pdfResultReport); 
	
//	public ValidusSalesforceComponents salesforceComponent = new ValidusSalesforceComponents(pdfResultReport);
	public Singapore_VUE_SalesforceComponents singapore_VUE_SalesforceComponents = new Singapore_VUE_SalesforceComponents(pdfResultReport);
	
	public void initializeRepository() throws Exception {		
		reportDetails.put("Test Script Name", this.getClass().getSimpleName());
		reportDetails.put("Test Script MyWorksshop Document ID", "Doc1234567");
		reportDetails.put("Test Script Revision No", "1");
		reportDetails.put("Test Author Name", "Nithin");
		reportDetails.put("Test Script Type", "User Acceptance Testing");
		reportDetails.put("Requirement Document ID of System", "Doc1234567");
		reportDetails.put("Requirement ID", "US2202");
	}
	
	@Parameters("TestcaseNo")
	@Test(description = "Scenario:1 - Test the functionality of Consumer with valid flow")
  public void f(String no) throws Throwable {
	  System.out.println("Entered in the test method..................");
	  try {
		pdfResultReport.readTestDataFile(System.getProperty("user.dir").replace("\\", "/")	+ "/Resources/Singapore_Testdata.xls", no);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  initializeRepository();
	  singapore_VUE_Components.openURL();
	  singapore_VUE_Components.CreateAccount_InvestorNEW();
	  singapore_VUE_SalesforceComponents.singaporeSalesforceIntegrationLogin();
	  singapore_VUE_SalesforceComponents.singaporeSalesforceOTP();
	  singapore_VUE_Components.login();
	  singapore_VUE_Components.navigateTocompanyIncorporatedInSingapore();
	  singapore_VUE_Components.fillcompanyIncorporatedInSingapore();
	  singapore_VUE_Components.uploadFiles_companyIncorporatedInSingaporeCitizen();
	  singapore_VUE_Components.clickContinue();
	  singapore_VUE_SalesforceComponents.salesforceApproval();
	  singapore_VUE_Components.logout();
	  singapore_VUE_Components.login();
	  singapore_VUE_Components.allliveFacilities();
	  singapore_VUE_Components.bankDetails();
	  singapore_VUE_SalesforceComponents.salesforcefund();
	  singapore_VUE_Components.switchToApp();
	  singapore_VUE_Components.allliveFacilities();
	  try {
		singapore_VUE_Components.fund();
	} catch (Exception e) {
		System.out.println("Unable to do funding..........");
	}
	  singapore_VUE_Components.withDraw();
	  singapore_VUE_SalesforceComponents.salesforceWithDraw();
	  singapore_VUE_Components.reports();
	  

	 
	  
	  
	
	}
}
